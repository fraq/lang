from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from ..models import * 
from django.contrib import messages
from common import css, template_filters
import logging
from django.contrib.auth.decorators import permission_required
from main.views import get_sp_or_redirect
from django.utils.decorators import method_decorator
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView 
from django.contrib.auth.mixins import PermissionRequiredMixin
from coach.forms import TipForm, TagForm

@permission_required('main.admin')
def integrity_check(request):
    log = logging.getLogger(__name__)

class TipDelete(PermissionRequiredMixin,DeleteView):
    permission_required = 'coach.delete_tip'
    model = Tip
    template_name = 'main/model_confirm_delete.html'
    success_url = reverse_lazy('coach:tip_list')

@permission_required(['coach.add_tip'],raise_exception=True)
def tip_list(request):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    tip_list = []
    for tip in Tip.objects.filter(native_language=sp.native_language).order_by('stage'):
        tags_REQUIRED = []
        tags_REJECTED = []
        for tag in tip.tags.filter(tiptag__relationship=True):
            tags_REQUIRED.append(tag.title_in(sp.native_language))
        for tag in tip.tags.filter(tiptag__relationship=False):
            tags_REJECTED.append(tag.title_in(sp.native_language))
            
        tip_list.append({
            'id': tip.id,
            'title': tip.title,
            'stage': tip.stage,
            'exposures': tip.exposures,
            'tags_REQUIRED': tags_REQUIRED,
            'tags_REJECTED': tags_REJECTED,
            'uptake': int(tip.uptake()*100),
            })
        
    context = {
        'tip_list': tip_list,
        }
    return render(request, 'coach/tip_list.html', context)

@permission_required(['coach.add_tip','coach.change_tip'],raise_exception=True)
def tip_create_update(request, id=None):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    
    available_tags = Tag.objects.tags_in(sp.native_language)
    initial = {
            # fields that appear as Form Fields
        'native_language':sp.native_language.id,        
        'tag_choices': Tag.objects.tags_in(sp.native_language),
        'selected_tags_INC': None,
        'selected_tags_EXC': None,
        'language_specific': False,
        'stage': '',
        'title': '',
        'id': id,
        'parent': None,
        'target_language': sp.course.target_language.id,
        }
    # context is info not in the form fields but used to render the template
    context = {
        'id': None if id == None else int(id),
#        'target_language': sp.course.target_language.id,
        }
    if id is not None: # update mode
        context['title'] = 'Update Coaching Tip'
        tip = get_object_or_404(Tip, id=id)
        selected_tags_INC = set()
        selected_tags_EXC = set()
        for tag in tip.tags.filter(tiptag__relationship=True):
            selected_tags_INC.add(tag.id)
        for tag in tip.tags.filter(tiptag__relationship=False):
            selected_tags_EXC.add(tag.id)       
        initial.update({
            'id': id,
            'title': tip.title,
            'text': tip.text,
            'homework_text': tip.homework_text,
            'parent': tip.parent,
            'selected_tags_INC': list(selected_tags_INC),
            'selected_tags_EXC': list(selected_tags_EXC),
            'language_specific': tip.language_specific,
            'stage': tip.stage,
            })
    else: # create mode
        context['title'] = 'Create a new Coaching Tip'
    if request.method == 'POST': # saving
        form = TipForm (request.POST, initial=initial)
        if form.is_valid():
            form.save(request)
            messages.add_message(request, messages.SUCCESS, 'Tip successfully saved')
            return redirect('coach:tip_list')
    else: # rendering initial form
        form = TipForm (None, initial=initial)
    context['form'] = form
    return render(request, 'coach/tip_create_update.html', context)

class TagDelete(PermissionRequiredMixin,DeleteView):
    permission_required = 'coach.delete_tag'
    model = Tag
    template_name = 'main/model_confirm_delete.html'
    success_url = reverse_lazy('coach:tag_list')

    def get_context_data(self, **kwargs):
        # This method allows us to add extra context information to the generic view
        context = super().get_context_data(**kwargs)
        context.update({
                'model_name':'Tag',
            })
        return context    



@permission_required(['coach.add_tag','coach.change_tag'],raise_exception=True)
def tag_create_update(request, id=None):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    
    initial = {
            # fields that appear as Form Fields
        'id':id,
        'title':None,
        'native_language':sp.native_language.id,     
        'parent':None,   
        }
    # context is info not in the form fields but used to render the template
    context = {
        }
    if id is not None: # update mode
        tag = get_object_or_404(Tag, id=id)
        initial.update({
            'id': id,
            'title': tag.title,
            'parent': tag.parent,
            })
        context.update({
            'id': id,
            'title':'Update Tag',
            })
    else: # create mode
        context['title'] = 'Create a new Tag'
    if request.method == 'POST': # saving
        form = TagForm (request.POST, initial=initial)
        if form.is_valid():
            form.save(request)
            messages.add_message(request, messages.SUCCESS, 'Tag successfully saved')
            return redirect('coach:tag_list')
    else: # rendering initial form
        form = TagForm (None, initial=initial)
    context['form'] = form
    return render(request, 'coach/tag_create_update.html', context)

class TagList(PermissionRequiredMixin,ListView):    
    permission_required = 'coach.content_creator'
    model = Tag
    template_name = 'main/model_list.html'

    def get_context_data(self, **kwargs):
        # This method allows us to add extra context information to the generic view
        context = super().get_context_data(**kwargs)
        context.update({
                'update_url':'coach:tag_update',
                'delete_url':'coach:tag_delete',
                'create_url':'coach:tag_create',
                'model_name':'Tag'
            })
        return context    

