from main.views.main import mode_completed
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

# perhaps each app can offer its own exercises
@login_required
def gram_offer_exercises(request):
    if request.POST:
        if form.is_valid():
            form.save()
            return mode_completed(request)
    return mode_completed(request)

# perhaps each app can offer a review of results from the previous week
@login_required
def gram_review_summary(request):
    if request.POST:
        if form.is_valid():
            form.save()
    return render(request, 'grammar/XX.html')

# perhaps each app can offer its own homework
@login_required
def gram_initial_homework(request):
    if request.POST:
        if form.is_valid():
            form.save()
    return render(request, 'grammar/XX.html')
