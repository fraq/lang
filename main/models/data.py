from django.db import models
from django.utils.translation import ugettext_lazy as _
from common import css
import logging
from datetime import date

class DataManager(models.Manager):
    def getval(self,key):
        # Returns None if entry not found
        # Cached data records are only good for 7 days, then they are deleted 
        try:
            record = Data.objects.get(key=key)
            if (date.today() - record.date).days > 7:
                record.delete()
                return None
            else:
                return css.literal(record.value)
        except:
            return None
    
    def create_or_update(self,key,value):
        try:
            record = Data.objects.get(key=key)
            record.value = value
            record.save()
        except:
            record = Data.objects.create(key=key,value=value)

class Data(models.Model):
    """
        Stores key/value pairs for the system, useful for caching data. A flexible storage system.
    """
    key = models.CharField(max_length=90, unique=True)
    value = models.CharField(null=True, max_length=200,blank=True)
    date = models.DateField(auto_now=True)
    objects = DataManager()

    def save(self, *args, **kwargs):
        super(Data, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Data Instance saved:"+str(self))

    def __str__(self):
        return  str([self.key, self.value])

    class Meta:
        ordering = ['date']