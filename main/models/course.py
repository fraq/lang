from django.db import models
from django.contrib.auth.models import User
from .language import Language
from .stage import Stage
from .data import Data
from django.utils.translation import ugettext_lazy as _
from common.MixinClasses import TranslatableMixin
import logging

class CourseManager(models.Manager):
    def get_health(self,course,native_language):
        """Retrieves the health for a course and native language"""
        key = "Course:{} Native:{} ".format(course.id,native_language.id)
        health = Data.objects.getval(key)
        if health is None:
            health = course.calculate_health(native_language)['score']
            Data.objects.create(key=key,value=int(health))
        return health

         
class Course(TranslatableMixin, models.Model):
    """
    A series of stages made up of grammars for teaching a particular **target_language**
    
    Each course can service multiple **native_languages** . course is available in all languages for which title has a translation  
    """
    QUALITY = {
        'LOW':{'value':15,'description':'Low'},
        'MEDIUM':{'value':30,'description':'Medium'},
        'GOOD':{'value':50,'description':'Good'},
        }
    
    title = models.ForeignKey('Sentence', on_delete=models.PROTECT, related_name='CourseTitle')
    target_language = models.ForeignKey('Language', related_name='target_courses', on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    last_edited = models.DateField(auto_now=True)
    objects = CourseManager()

    def long_title(self,native_language):
        return "{} ({}->{})".format(self.title_in(native_language),
                native_language.title_in(native_language),
                self.target_language.title_in(native_language)
                )

    def save(self, *args, **kwargs):
        super(Course, self).save(*args, **kwargs)
        if not self.stage_set.exists():
            Stage.objects.create(position=1,course=self,author=self.author)
        log = logging.getLogger(__name__)
        log.info("Model Course Instance saved:"+str(self))

    def lowest_stage(self):
        """Returns the first stage starting point"""
        return self.stage_set.order_by("position").first()

    def get_description(self,score):
        """Returns a text description of the score"""
        if score > self.QUALITY['GOOD']['value']:
            description = self.QUALITY['GOOD']['description']
        if score > self.QUALITY['MEDIUM']['value']:
            description = self.QUALITY['MEDIUM']['description']
        if score > self.QUALITY['LOW']['value']:
            description = self.QUALITY['LOW']['description']
        else:
            description = 'Insufficient for use'
        return description
        
    def calculate_health(self,native_language):
        """Computes and returns a health score for the course based on how many populated stages exist"""
        messages = []
        stage_count = self.stage_set.count() 
        score = 0
        for stage in self.stage_set.all():
            grammar_count = stage.grammar_set.count()
            healthy_grammars = 0 # num of grammars with sufficient questions
            stage_health = 0
            for grammar in stage.grammar_set.all():
                question_count = grammar.question_set.filter(native_language=native_language).count()
                grammar_health = min(question_count,10)/10 # each grammar should have at least 10 Qs
                if question_count == 0:
                    messages.append("Course:"+str(self)+" Stage:"+str(stage)+" Grammar:"+str(grammar)+" has no questions")
                stage_health += grammar_health/max(grammar_count,6) # each stage should have at least 6 grammars
            if grammar_count == 0:
                messages.append("Course:"+str(self)+" Stage:"+str(stage)+" has no registered grammars")
            score += stage_health/max(stage_count,8) # each course should have at least 8 stages
        if stage_count == 0:
            messages.messages.append("Course:"+str(self)+" has no stages")   
        return {'score':score*100, 'messages':messages}

    class Meta:
        ordering = ['-last_edited']
        permissions = (
            ("content_creator", "Can create,read,update,delete"),
        )
        