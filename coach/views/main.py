from main.views.main import mode_completed
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from coach.models import Tip, TipEncounter
from main.models import Profile
from main.views import get_sp_or_redirect, mode_completed, mode_not_completed
import logging
from grammar.models import FocusGrammarProgress
from django.contrib import messages
from common import css

@login_required
def holistic_report(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": holistic_report started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    if request.POST:
        if form.is_valid():
            form.save()
            return mode_completed(request)
    return mode_completed(request)


@login_required
def follow_up_homework(request):
    log = logging.getLogger(__name__)
    log.debug("User" + request.user.username + ": follow_up_homework started")
#     sp,request = get_sp_or_redirect(request)
#     if not sp:
#         return redirect('main:select_course')
# 
#     # Ask followup questions on the coaching tips and homework that was assigned last check-in
#     # Decide which homework has to stay in the system
#     # Decide which tips to bookmark or archive.
#     # On continuation, archive tips from last time, making room for new ones.
# 
#     current_tip_encs = TipEncounter.objects.filter(sp=sp,status=TipEncounter.CURRENT)
#     current_tips = Tip.objects.filter(id__in=current_tip_encs.value_list('tip',flat=True))
#     initial = {
#         'current_tips':current_tips,        
#         }
#     
#     if request.POST:
#         form = FollowUpHomeworkForm(request.POST, initial=initial)
#         if form.is_valid():
#             form.save()
#             return mode_completed(request)
#             
#             # Amongst other tasks, this function must move all entries in Encounter model relevant to user's sp from Current to another status.
#             for tip_enc in current_tip_encs:
#                 if tip_enc.status == TipEncounter.BOOKMARKED:
#                     tip_enc.status = TipEncounter.ARCHIVED
#                 elif tip_enc.status == TipEncounter.CURRENT:
#                     tip_enc.status = TipEncounter.STALE # keep for re-presentation until they bookmark            
#                 tip_enc.save()
# 
#             return mode_completed(request)
#     context = {}
#     return render (request, 'coach/follow_up_homework.html', context)


    return mode_completed(request)

@login_required
def offer_coaching_tips(request):
    log = logging.getLogger(__name__)
    log.debug("User" + request.user.username + ": offer_coaching_tips started")

    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

# Work out suitable Tips
# Present to user. they either bookmark or reject each
# save in TipEncounter model user responses, and in Profile which have been presented today; this records learning focus so it isn't repeated following week and also for working out homework 


    if request.POST:
        return mode_completed(request)
    else:
        # determine overall sp.stage
        fp = FocusGrammarProgress.objects.get(sp=sp)
        sp.stage = fp.stage
        sp.save()

        # sanity check
        encs_still_current = TipEncounter.objects.filter(sp=sp, status=TipEncounter.CURRENT)
        if encs_still_current.exists():
            #log.error("User" + request.user.username + ": offer_coaching_tips: There should not be current tips remaining at this stage. ")
            for tip_enc in encs_still_current:
                tip_enc.status = TipEncounter.BOOKMARKED
                tip_enc.save()

        selected_tips = Tip.objects.select(sp) # choose new tips
        selected_tip_ids = []
        selected_exercise_ids = []
        for tip in selected_tips:
            if tip.text is not None:
                if len(tip.text) > 2:
                    selected_tip_ids.append(tip.id) 
            if tip.homework_text is not None:
                if len(tip.homework_text) > 2:
                    selected_exercise_ids.append(tip.id) 
        Profile.objects.create_or_update(sp,'selected_tip_ids',value=selected_tip_ids)
        Profile.objects.create_or_update(sp,'selected_exercise_ids',value=selected_exercise_ids)
        if selected_tip_ids:
            context = {'selected_tips': Tip.objects.filter(id__in=selected_tip_ids)}
            return render(request, 'coach/view_selected_tips.html', context)
        else:            
            # No tips for today
            return mode_completed(request)

@login_required
def offer_exercises(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": offer_exercises started")

    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    if request.POST:
        messages.add_message(request, messages.SUCCESS, 'That is the end of your session for today. Revisit this page at any time this week to review your coaching tips and homework exercises.')
        return mode_completed(request)
    else:
        selected_exercise_ids = Profile.objects.getval(sp,'selected_exercise_ids')
        if selected_exercise_ids: # previously populated by offer_coaching_tips view            
            context = {'selected_exercises': Tip.objects.filter(id__in=selected_exercise_ids)}
            return render(request, 'coach/view_selected_exercises.html', context)
        else:            
            # No homework exercises for today
            return mode_completed(request)