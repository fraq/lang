from django.db import models
import logging
    
class TipEncounter(models.Model):
    """
    Tracks which users have viewed which tips and whether the user bookmarked the tip
    """

    CURRENT = 'C' # Currently being viewed and actioned
    BOOKMARKED = 'B' # Selected by user to save/archive/bookmark
    DELAYED = 'D' # Felt it was too advanced, wanted to come back later
    REJECTED = 'R' # Rejected
    PREVIOUSLY = 'P' # Was already being implemented prior to viewing the tip (i.e. "I already do this!") 
    STATUS_CHOICES = ((CURRENT, 'Current'), (BOOKMARKED, 'Bookmarked'), (DELAYED, 'Delayed'), (REJECTED, 'Rejected'), (PREVIOUSLY, 'Previously implemented'))   
    
    tip = models.ForeignKey('Tip', on_delete=models.CASCADE)
    sp = models.ForeignKey('main.StudentProgress', null=True, on_delete=models.SET_NULL)
    date = models.DateField(auto_now=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, db_index=True, default=CURRENT, help_text="How the user has responded to the proffered tip") 
    