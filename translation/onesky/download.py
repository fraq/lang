from wrapper import OneSkyWrapper
import os

ONESKY_KEY = os.environ['ONESKY_KEY']
ONESKY_SECRET = os.environ['ONESKY_SECRET']
ONESKY_PROJECT = os.environ['ONESKY_PROJECT']

wrapper = OneSkyWrapper(ONESKY_KEY, ONESKY_SECRET, ONESKY_PROJECT)
wrapper.getTranslations()



