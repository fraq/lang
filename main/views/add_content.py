from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from main.models.stage import Stage
import logging
from .main import mode_completed, mode_not_completed, get_sp_or_redirect

@permission_required('main.add_stage')
def add_stage(request):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    course = sp.course    
    last_pos = course.stage_set.order_by("position").last().position
    new_stage = Stage.objects.create(position=last_pos+1,course=course,author=request.user)
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + " added stage "+ str(new_stage))
    return redirect('grammar:create_dashboard')
