#!/usr/bin/env bash

python translation/onesky/download.py
python translation/translationWithGoogle.py

for local in `ls locale`
do
    if [ 'django.pot' != "$local" ]; then
        echo "Compiling lang $local"
        python manage.py compilemessages -l $local
    fi
done
