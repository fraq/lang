WINNER = 1
LOSER = 0
DRAW = 0.5
MAX_VARIANCE = 40.0

def determine_scores (actor_list):
    # actor_list: a list of actor lists. The first element being the winner actor and last being the loser.
    #     each actor element is a list, the first being the current ELO score, the second being the K-variance factor
    # returns: an identically structured list with updated ELO and K-variance per actor
    
    rescored = [] # list of rescored actors for output
    num_actors = len(actor_list)
    assert num_actors > 1 # must be passed more than one actor       
    R = {} # define dictionary
    K = {}
    sum = 0

    for idx, actor in enumerate(actor_list):
        # R(1) = 10**(1elo/400)
        # R(2) = 10**(2elo/400)
        R[idx] = 10**(actor.score/400)
        sum = sum + R[idx]       
        K[idx] = actor.variance

    # score = 1 for win, 0 for loss, middle players get 0.5/(number of middle players)
    if num_actors > 2:
        neutral_score = 0.5/(num_actors-2)
    else:
        neutral_score = DRAW

    for idx, actor in enumerate(actor_list):             
        if actor.position == DRAW:
            score = neutral_score # others draw
        else: 
            score = actor.position

        actor.score = actor.score + K[idx] * (score -(R[idx] / sum))
        actor.variance = actor.variance * 0.9 # decrease K-variance by 10% after a contest

    return actor_list
    
class Actor:
    def __init__(self, elo_score, variance, position):
        self.score = elo_score
        self.variance = variance
        self.position = position

