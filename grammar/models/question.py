from django.db import models
from django.contrib.auth.models import User
from common import massage
import logging
from tinymce import HTMLField
from django.core.exceptions import ValidationError

# Questions are pairs of sentences. One being the front (the question), and the other being
# the back (the answer). Which is written in the target_language and which in the 
# native_language is arbitrary. 
# Questions can be marked as key_cases, ensuring they are displayed first to a student.
# Also they can provide a hint (displayed when the front is shown), and an explanation
# (displayed when the back is shown.) A question belongs to a single grammar, even though
# its sentences could contain multiple grammars. The question's grammar determines when the
# question will appear.
# The question score starts as that of the grammar's base score, but with time
# increases or decreases to reflect the question's unique difficulty as determined by student
# success in successfully answering it.

class QuestionQuerySet(models.QuerySet):
    def best_quality(self):
    # Questions should be selected by an algorithm. Low-tested Qs prioritised to get feedback
    # and to determing their ELOs, and prioritising Qs with approval by users (low/no complaints).
        return self.first() # placeholder

# class QuestionManager(models.Manager):
# #     def get_queryset(self):
# #         return QuestionQuerySet(self.model, using=self._db)
# 
#     def best_quality(self):
#     # Questions should be selected by an algorithm. Low-tested Qs prioritised to get feedback
#     # and to determing their ELOs, and prioritising Qs with approval by users (low/no complaints).
# #        return self.get_queryset().first() # placeholder
#         return super(QuestionManager, self).get_queryset().first()

class Question(models.Model):
    front = models.ForeignKey('main.Sentence', related_name='question_fronts', help_text='question sentence', on_delete=models.CASCADE)
    back = models.ForeignKey('main.Sentence', related_name='question_backs', help_text='answer sentence', on_delete=models.CASCADE) 
    target_language = models.ForeignKey('main.Language', related_name='question_target_language', on_delete=models.CASCADE)
    native_language = models.ForeignKey('main.Language', related_name='question_native_language', on_delete=models.CASCADE)
    hint = HTMLField(help_text="Written in the native language, a hint that directs a user of this language towards selecting the correct answer. Useful where the target language may allow multiple equivalents",blank=True) 
    explanation =  HTMLField(help_text="Written in the native language, an explanation to peculiarities in the answer.",blank=True)
    grammar = models.ForeignKey('Grammar' , null=True, blank=True, db_index=True, on_delete=models.SET_NULL)
    simplified = models.CharField(max_length=200, help_text="massaged entry text", blank=True, null=True) # internal hashprocessing to make comparing similar entries easier
    score = models.IntegerField(null=True, blank=True, db_index=True, help_text="starts as grammar ELO. If users continue to find this Q hard, this will be indicated by this score creeping upwards.")    
    variance = models.IntegerField(default=250) # note - actual applied variance is modified by the student's variance, the greater the student's variance, the less impacted the question should be by that student's result
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    last_edited = models.DateField(auto_now=True)
    approval = models.NullBooleanField(default=None,blank=True, help_text="A summary of ratifications, whether this question has been approved by native speakers")
    ratifications = models.ManyToManyField('main.Ratification', help_text="Tracks completed ratifications", blank=True)

#    objects = QuestionManager()
    objects = QuestionQuerySet.as_manager() 
    
    def rescore(self, score):
        # updates question ELO score
        self.score = max(score, 0)
        self.variance = self.variance * 0.9
        self.save()

    def native_front(self):
        return (self.front.language == self.native_language)

    def nativesentence(self):
        if self.native_front():
            return self.front
        else:
            return self.back

    def targetsentence(self):
        if self.native_front():
            return self.back
        else:
            return self.front

    def save(self, *args, **kwargs):
        # Primarily this is to be called when the instance is initially created. setting the inital score value.
        # But re-saving questions can also be useful to update simplified field if the hash method changes.

        # find highest grammar associated with the targetsentence
        self.grammar = self.targetsentence().grammars.filter().order_by('-score').first()
        if self.grammar is None:
            raise ValidationError('At least one grammar is required to be provided')
        self.simplified = massage.simplify(
            self.back.text,
            native=self.native_language.iso693_2,
            target=self.target_language.iso693_2,
        )
        if self.score is None:
            self.score = self.grammar.score # by default, questions start off at their grammar's base score
        # save as the question's grammar        
        super(Question, self).save(*args, **kwargs)

    def __str__(self):
        return  str([self.front, self.back])

    class Meta:
        ordering = ['-last_edited']
