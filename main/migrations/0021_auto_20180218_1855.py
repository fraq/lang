# Generated by Django 2.0.2 on 2018-02-18 15:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_remove_studentprogress_last_visit'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='entry',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='profile',
            name='value',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
    ]
