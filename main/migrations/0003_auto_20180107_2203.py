# Generated by Django 2.0.1 on 2018-01-07 19:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20180102_1113'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ['-last_edited'], 'permissions': (('content_creator', 'Can create,read,update,delete'),)},
        ),
        migrations.AlterField(
            model_name='sentence',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
