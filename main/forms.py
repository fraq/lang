from django import forms
import logging
from main.models import Language, Profile, Course, StudentProgress, Student, Data
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from tinymce.widgets import TinyMCE
from common import css
from django.shortcuts import get_object_or_404
import json

class CheckInForm(forms.Form):
    """Additional required params: date, elapsed_days
    """
    
    ReportDate = forms.CharField(widget=forms.HiddenInput(),required=True)
    ElapsedDays = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    
    InputHours= forms.FloatField(initial=0,required=True)
    InputActivities= forms.CharField(required=False, widget=TinyMCE(mce_attrs={'toolbar': False, 'required': False, 'autoresize_max_height': 130}))
    StudyHours= forms.FloatField(initial=0,required=True)
    StudyActivities= forms.CharField(required=False, widget=TinyMCE(mce_attrs={'toolbar': False, 'required': False, 'autoresize_max_height': 130}))
    CommunityHours= forms.FloatField(initial=0,required=True)
    CommunityActivities= forms.CharField(required=False, widget=TinyMCE(mce_attrs={'toolbar': False, 'required': False, 'autoresize_max_height': 130}))
    factors= forms.CharField(required=False, widget=TinyMCE(mce_attrs={'cols': 20, 'toolbar': False, 'required': False, 'autoresize_max_height': 130}))
    motivation= forms.CharField(required=True, widget=forms.TextInput(), 
                                   error_messages= {'required': 'Select an option describing your current motivation level'})
    motivation_reason= forms.CharField(required=False, widget=TinyMCE(mce_attrs={'toolbar': False, 'required': False, 'autoresize_max_height': 130}))
    help_request= forms.CharField(required=False, widget=TinyMCE(mce_attrs={'toolbar': False, 'required': False, 'autoresize_max_height': 130}))
    embarrassing_story= forms.CharField(required=False, widget=TinyMCE(mce_attrs={'required': False, 'autoresize_max_height': 130}))

    def save(self,request):
        log = logging.getLogger(__name__)
        data = self.cleaned_data
        entry = 'WeCareReport:Date:{}'.format(data['ReportDate'])
        text = json.dumps(self.cleaned_data, default=lambda o: o.__dict__, indent=4)
        sp = StudentProgress.objects.get(id=request.session['sp_pk'])
        Profile.objects.create_or_update(sp,entry=entry,text=text)

class SignUpCourseForm(forms.Form):    
    selection_code = forms.ChoiceField(choices=(),widget=forms.RadioSelect,required=True)
            
    def __init__(self, *args, **kwargs):
        course_choices = kwargs.pop('course_choices') 
        super(SignUpCourseForm, self).__init__(*args, **kwargs) 
        self.fields['selection_code'].choices = course_choices                 

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=150)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

class ContinueForm(forms.Form):
    """A simple form consisting of a continue button 
    """

    def __init__(self, *args, **kwargs): 
        super(ContinueForm, self).__init__(*args, **kwargs) 
            
    def save(self,request):
        data = self.cleaned_data
        log = logging.getLogger(__name__)
        log.info("User " + request.user.username + " submitted ContinueForm")
        
class ProfileForm(forms.Form):
    # Profile fields that get saved in profile.value (limited to 30 char, searchable)
    # pass redo_all_entries=True for a registered user, and profile fields will be wiped, allowing re-entry - UNTESTED
    
    value_fields = ['hour_commitment','past_experience','input_methods','lang_accessibility','course','human_coach_exists','human_coach_name','human_coach_email']
    # Profile fields that get saved in profile.text (longer, unsearchable)
    text_fields = ['tried_so_far','holding_back']
    student_fields = ['native_languages']
    all_profile_fields = value_fields + text_fields + student_fields

    def __init__(self, *args, **kwargs): 
        request = kwargs.pop('request')

        if 'redo_all_entries' in kwargs:
            redo_all_entries = kwargs.pop('redo_all_entries')
        else:
            redo_all_entries = False

        super(ProfileForm, self).__init__(*args, **kwargs) 
        
        #### Populate existing_data        
        existing_data = {}
        existing_data['courses'] = []
        for field in self.all_profile_fields:
            existing_data[field] = None
        if 'sp_pk' in request.session:
            sp = StudentProgress.objects.get(id=request.session['sp_pk'])
            existing_data['courses'] = list(StudentProgress.objects.filter(user=sp.user).values_list('course', flat=True))
            if not redo_all_entries:
                for field in self.all_profile_fields:                
                    existing_data[field] = Profile.objects.getval(sp,field)
                existing_data['native_languages'] = css.literal(Student.objects.get(user=request.user).native_languages)
        else:
            for field in self.all_profile_fields:                
                if field in request.session['profile_data']:
                    existing_data[field] = css.literal(request.session['profile_data'][field])

        #### Create form fields
        if existing_data['course']:
            [course_id,native_id] = css.literal(existing_data['course']) 
            course = Course.objects.get(id=course_id)
            native = Language.objects.get(id=native_id) 
            course_target_lang = course.target_language.title_in(native)
            request.session['target_language'] = course_target_lang
        
        # native_languges field
        if not existing_data['native_languages']:
            initial = existing_data['native_languages']
            choices = []
            for lang in Language.objects.all():
                choices.append((lang.id, str(lang.title)))
            self.fields['native_languages'] = forms.MultipleChoiceField(choices = choices, 
                                                                  widget=forms.CheckboxSelectMultiple,
                                                                  initial = initial,
                                                                  label = 'In which language(s) are you natively fluent?', 
                                                                  required= not existing_data['native_languages'])

        # human_coach_exists
        if (existing_data['human_coach_exists'] == None):
            initial = None
            choices = ((True,"Yes"),
                       (False,"No"),
                       )
            self.fields['human_coach_exists'] = forms.ChoiceField(choices = choices,
                                                          widget=forms.RadioSelect,
                                                          initial = initial,
                                                          label = 'Do you have a person who is going to be coaching you and would like to receive reports regarding your progress on Grow Fast Grow Deep?', 
                                                          required= (existing_data['human_coach_exists'] == None))       

        # course_selection
        if (not existing_data['course']) and existing_data['native_languages']:
            initial = None
            
            possible_courses = []
            # eliminate courses if the student is already studying
            for course in Course.objects.exclude(id__in=existing_data['courses']):
                for native_lang_id in existing_data['native_languages']:
                    native_language = Language.objects.get(id=native_lang_id)
                    course_health_score = Course.objects.get_health(course,native_language)
                    if course_health_score > Course.QUALITY['LOW']['value']:
                        if course.target_language !=  native_language:
                            course_key = "{},{}".format(course.id,native_lang_id)
                            target_lang = course.target_language.title_in(native_language)
                            native_lang_title = native_language.title_in(native_language)
                            course_title = course.long_title(native_language)
                            possible_courses.append((course_key, course_title))            
            self.fields['course'] = forms.ChoiceField(choices = possible_courses,
                                                          widget=forms.RadioSelect,
                                                          initial = initial,
                                                          label = 'Which language would you like to study?', 
                                                          required= not existing_data['course'])

        # human_coach_name
        if (not existing_data['human_coach_name']) and (existing_data['human_coach_exists'] == 'True'):
            initial = None
            self.fields['human_coach_name'] = forms.CharField(max_length = 70,
                                                    label="Your coach's first name")

        # human_coach_email
        if (not existing_data['human_coach_email']) and (existing_data['human_coach_exists'] == 'True'):
            initial = None
            self.fields['human_coach_email'] = forms.EmailField(max_length = 70,
                                                    label="Your coach's email")
        
        # ask hour commitment
        if (not existing_data['hour_commitment']):
            initial = existing_data['hour_commitment']
            choices = ((30,"30+ hours a week - I want to study it fulltime until I reach my fluency goals"),
                       (15,"15 hours a week - I have other responsibilities I cannot escape, but language acquisition is at least my second if not first priority"),
                       (7,"7 hours a week - I will be carving out regular daily time to study regularly and progress"),
                       (2,"2 hours or less a week - I can only occasionally be putting time aside for language learning"),
                       )
            self.fields['hour_commitment'] = forms.ChoiceField(choices = choices,
                                                          widget=forms.RadioSelect,
                                                          initial = initial,
                                                          label = 'How much time are you willing to give to language learning on a weekly basis?', 
                                                          required= not existing_data['hour_commitment'])        
        # ask prior experience
        if (not existing_data['past_experience']) and (existing_data['course']):
            initial = existing_data['past_experience']
            choices = ((0,"None at all"),
                       (1,"A little, I know some words and phrases"),
                       (2,"I have been studying it for less than a year"),
                       (3,"I have some confidence in the language"),
                       (4,"I am already quite fluent"))
            self.fields['past_experience'] = forms.ChoiceField(choices = choices,
                                                          widget=forms.RadioSelect,
                                                          initial = initial,
                                                          label = 'Have you any prior experience in learning {}?'.format(course_target_lang), 
                                                          required= not existing_data['past_experience'])        
        # ask current input
        if (not existing_data['input_methods']) and (existing_data['course']):
            initial = existing_data['input_methods']            
            choices = (("LangHelper","I arrange times to practice regularly with a native speaker"),
                    ("Class","I attend a language class"),
                    ("Tutor","I employ a private teacher"),
                    ("Textbook","I am studying a language textbook on my own initiative"),
                    ("None","No I haven’t started yet"))
            self.fields['input_methods'] = forms.MultipleChoiceField(choices = choices,
                                                          widget=forms.CheckboxSelectMultiple,
                                                          initial = initial,
                                                          label = 'Are you currently receiving language learning input in any form?', 
                                                          required= not existing_data['input_methods'])
        # ask lang_accessibility
        if (not existing_data['lang_accessibility']) and (existing_data['course']):
            initial = existing_data['lang_accessibility']            
            choices = (("Everywhere","Yes, as soon as I go out the door"),
                    ("Pockets","Yes, there are pockets of people I can easily visit who speak it"),
                    ("No","No, if I looked hard maybe I could find someone who speaks it"))
            self.fields['lang_accessibility'] = forms.ChoiceField(choices = choices,
                                                          widget=forms.RadioSelect,
                                                          initial = initial,
                                                          label = 'Are you currently living in a country where {} is commonly spoken around you?'.format(course_target_lang), 
                                                          required= not existing_data['lang_accessibility'])
        # ask tried so far
        if (not existing_data['tried_so_far']) and (existing_data['past_experience'] in ['1','2','3','4']):
            self.fields['tried_so_far'] = forms.CharField(widget=TinyMCE(mce_attrs={'cols': 20, 'required': False}),
                                                          initial = existing_data['tried_so_far'],
                                                          label = 'Tell us a little about what techniques you have tried so far in language learning',
                                                          required=False,
                                                          )
        # ask what is holding back
        if (not existing_data['holding_back']) and (existing_data['past_experience'] in ['1','2','3','4']):
            self.fields['holding_back'] = forms.CharField(widget=TinyMCE(mce_attrs={'cols': 20, 'required': False}),
                                                          initial = existing_data['tried_so_far'],
                                                          label = 'In your opinion, what is most holding you back from progressing in your language learning?',
                                                          required=False,
                                                          )

    def save(self,*args, **kwargs):
        request = kwargs.pop('request')
        mode = kwargs.pop('mode')
        if mode == 'save_to_session':
            sp = False
            data = self.cleaned_data
        elif mode == 'save_to_user':
            sp = StudentProgress.objects.get(id=request.session['sp_pk'])
            data = self.cleaned_data
        elif mode == 'transfer':
            sp = StudentProgress.objects.get(id=request.session['sp_pk'])
            data = request.session['profile_data']

        if sp and 'course' in data: 
                [course_id,native_id] = data['course'].split(',')
                course = Course.objects.get(id=course_id)
                native = Language.objects.get(id=native_id) 
                if not StudentProgress.objects.filter(course=course, user=sp.user).exists():
                    sp = StudentProgress.objects.create(user=request.user, course=course, native_language=native)
                    request.session['sp_pk'] = sp.id

        for key in self.value_fields:
            if key in data:
                if sp:
                    Profile.objects.create_or_update(sp=sp,entry=key,value=data[key])
                else:
                    request.session['profile_data'][key] = data[key]
                
        for key in self.text_fields:
            if key in data:
                if sp:                    
                    Profile.objects.create_or_update(sp=sp,entry=key,text=data[key])
                else:
                    request.session['profile_data'][key] = data[key]   
        for key in self.student_fields:
            if key in data:
                if sp:
                    if key == 'native_languages':
                        student = Student.objects.get(user=request.user)
                        student.native_languages = data['native_languages']
                        student.save()
                else:
                    if key == 'native_languages':
                        request.session['profile_data']['native_languages'] = data['native_languages']                        
        return request
        