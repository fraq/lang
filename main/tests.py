from django.test import SimpleTestCase, TestCase, RequestFactory, Client
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser, User
from main.models import *
from common import css


# Create your tests here.

class Language_Tests(TestCase):
        
    def setUp(self):
        from django.core.management import call_command
        call_command('loaddata', 'testdata.json', verbosity=0)

    def test_redirect_for_multiple_sps(self):
        """ admin user has multiple sps, when logging in confirm that they can register"""
        c = Client()
        c.login(username='admin',password='qweqweqwe')        
        # attempt to access a page
        response = c.get(reverse('coach:tip_list'),follow=True)
        # since admin has multiple sps, is redirected and asked to choose a course        
        self.assertContains(response, 'selection_code')
        self.assertContains(response, '98')
        response = c.post(reverse('main:select_course'), {'selection_code': 98},follow=True)
        # now can visit another page, like tiplist
        response = c.get(reverse('coach:tip_list'),follow=True)
        self.assertEqual(response.status_code, 200)


class Test_Loading_of_all_views(TestCase):
    """ Check each view loads without errors """
    @classmethod
    def setUpTestData(cls):
        from django.core.management import call_command
        call_command('loaddata', 'testdata.json', verbosity=0)

    def test_each_view(self):
        """Loads all possible views, checks there is the correct response""" 
        # (view_name, get_params, post_params)
        Admin_views = [
                ('main:test',None,None),            
                ('main:integrity_check',None,None),
            ]
        
        SuperCreator_views = [
                ('grammar:gramconcept_delete',{'pk':5},None), # different id to minimise chance of tests clashing
                ('grammar:grammar_delete',{'pk':2},None), # different id to minimise chance of tests clashing
#                ('grammar:grammar_restage',{'pk':3},{'stage':6}),            
            ]
        
        ContentCreator_views = [
                ('main:switchboard',None,None),
                ('main:review_summary',None,None),
                ('main:select_course',None,None),
                ('main:signup_course',None,None),
                ('main:reset_all',None,None),
                ('main:add_stage',None,None),
                #('logout_view',None,None),
                ('coach:tip_create',None,None),
                ('coach:tip_list',None,None),
                ('coach:tag_create',None,None),
                ('coach:tag_list',None,None),
                ('grammar:gramconcept_list',None,None),
                ('grammar:gramconcept_create',None,None),
                ('grammar:gramconcept_update',{'pk':1},None),
                ('grammar:create_dashboard',None,None),             
                ('grammar:gram_create',None,None),
                ('grammar:new_question',None,None),
                ('grammar:ratify_questions',None,None),
                ('grammar:view_ratifications',None,None),
                ('grammar:initial_grammar_test',None,None),
                ('grammar:run_initial_placement',None,None),
#                 ('grammar:weekly_assessment_schedule_qs',None,None),
#                 ('grammar:weekly_assessment_ask_qs',None,None),
#                 ('grammar:question_score',None,None),
#                 ('grammar:ajax_submit_answer',None,None),
#                 ('grammar:weekly_assessment_report',None,None),
                ('coach:tip_update',{'id':1},None),
                ('coach:tip_delete',{'pk':2},None), # different id to minimise chance of tests clashing
                ('coach:tag_update',{'id':1},None),
                ('coach:tag_delete',{'pk':2},None), # different id to minimise chance of tests clashing
                ('grammar:gram_update',{'pk':1},None),
                ('grammar:new_question_for_grammar',{'grammar_id':1},None),
                ('grammar:new_question_from_sentence',{'sentence_id':1},None),
                ('grammar:update_question',{'question_id':1},None),
                ('grammar:question_delete',{'pk':2},None),
                ('grammar:example_questions',{'grammar_id':1},None),
               ]
        anon_views = ['main:profile_user','splash','main:getting_started',
                      'main:register_new_user']

        """
        Testing all views for anonymous user
        """
        anon_user = Client()
        logged_in_user = Client()
        logged_in_user.login(username='ContentCreator',password='qweqweqwe') 
        admin_user = Client()
        admin_user.login(username='admin',password='qweqweqwe') 
        # since admin has multiple sps, is redirected and asked to choose a course        
        admin_user.post(reverse('main:select_course'), {'selection_code': 98},follow=True)
        # now can visit another page, like tiplist
        super_user = Client()
        super_user.login(username='SuperCreator',password='qweqweqwe') 
        
        
        print('anon_views')
        for view in anon_views:
            print (view)            
            a_response = anon_user.get(reverse(view),follow=True)
            self.assertEqual(a_response.status_code, 200)
            self.assertNotContains(a_response, 'Log-in')

            if view in ['main:getting_started']:
                print('skipping')
                continue
            else:
                l_response = logged_in_user.get(reverse(view),follow=True)
                self.assertEqual(l_response.status_code, 200)
                self.assertNotContains(l_response, 'Log-in')

        print('ContentCreator_views')
        for view_tuple in ContentCreator_views:
            (view,get_params,post_params) = view_tuple
            print("VIEW:{} GET:{} POST:{}".format(view,get_params,post_params))
            url = reverse(view, kwargs=get_params)
            if post_params is None:
                a_response = anon_user.get(url)
            else:
                a_response = anon_user.post(url,post_params)
            self.assertNotEqual(a_response.status_code, 200)
            if a_response.status_code == 302: # redirect
                a_response = anon_user.get(url,follow=True)
                self.assertEqual(a_response.status_code, 200)
                if view in ['main:switchboard']:
                    self.assertContains(a_response, 'Get started')
                else:
                    self.assertContains(a_response, 'Log-in')

            if view in ['main:review_summary','main:reset_all']:
                print('skipping')
                continue
            else:
                if post_params is None:
                    l_response = logged_in_user.get(reverse(view, kwargs=get_params),follow=True)
                else:
                    l_response = logged_in_user.post(reverse(view, kwargs=get_params),post_params,follow=True)    
                self.assertEqual(l_response.status_code, 200)
                self.assertNotContains(l_response, 'Log-in')

        print('SuperCreator_views')
        for view_tuple in SuperCreator_views:
            (view,get_params,post_params) = view_tuple
            print("VIEW:{} GET:{} POST:{}".format(view,get_params,post_params))
            url = reverse(view, kwargs=get_params)

            if post_params is None:
                a_response = anon_user.get(url)
                l_response = logged_in_user.get(url)
                su_response = super_user.get(url)
            else:
                a_response = anon_user.post(url,post_params)
                l_response = logged_in_user.post(url,post_params)    
                su_response = super_user.post(url,post_params)
            self.assertNotEqual(a_response.status_code, 200)
            self.assertNotEqual(l_response.status_code, 200)
            self.assertEqual(su_response.status_code, 200)
    
        print('Admin_views')
        for view_tuple in Admin_views:
            (view,get_params,post_params) = view_tuple
            print("VIEW:{} GET:{} POST:{}".format(view,get_params,post_params))
            url = reverse(view, kwargs=get_params)

            if post_params is None:
                a_response = anon_user.get(url)
                l_response = logged_in_user.get(url)
                admin_response = admin_user.get(url)
            else:
                a_response = anon_user.post(url,post_params)
                l_response = logged_in_user.post(url,post_params)    
                admin_response = admin_user.post(url,post_params)
            self.assertNotEqual(a_response.status_code, 200)
            self.assertNotEqual(l_response.status_code, 200)
            self.assertEqual(admin_response.status_code, 200)
        
    
    def test_logout(self):
        """ Logout view works fine """
        response = self.client.get(reverse('logout_view'),follow=True)
        self.assertContains(response, 'Logged out')   
        self.assertEqual(response.status_code, 200)
        
    def test_switchboard(self):

        # this list needs to be copied and kept current...
        from main.views.report import profile_user,website_induction,check_in,register_new_user
        from main.views.assess import can_do_statements,choose_learning_area,listening_comprehension,reading_comprehension,vocab_level
        from grammar.views.assess import initial_grammar_test,weekly_grammar_level  
        from grammar.views.coaching import initial_grammar_report, weekly_grammar_report
        from main.views.homework import review_summary
        from coach.views.main import offer_coaching_tips, offer_exercises, follow_up_homework, holistic_report

        switch_lookup = {
            # we repeat mode and the identical function name because to use eval() is considered extremely dangerous security vulnerability
    
                # new user
                ('REPORT','register_new_user'):(register_new_user,'REPORT','website_induction'),
                ('REPORT','website_induction'):(website_induction,'ASSESS','choose_learning_area'),
    
                # returning user
                ('',''):(profile_user,'REPORT','profile_user'),    # profile_user is also accessed via splash_page for anonymous users. anon users on completion are redirected to register_new_user 
                ('REPORT',''):(profile_user,'REPORT','profile_user'),    # profile_user is also accessed via splash_page for anonymous users. anon users on completion are redirected to register_new_user 
                ('REPORT','profile_user'):(profile_user,'REPORT','check_in'),
                ('REPORT','check_in'):(check_in,'REPORT','follow_up_homework'),
                ('REPORT','follow_up_homework'):(follow_up_homework,'ASSESS','choose_learning_area'), 
    
                # choose_learning_area redirects to one of the following
                ('ASSESS',''):(choose_learning_area,'ASSESS','choose_learning_area'),
                ('ASSESS','choose_learning_area'):(choose_learning_area,'ASSESS','choose_learning_area'),
                ('ASSESS','can_do_statements'):(can_do_statements,'ASSESS','choose_learning_area'),                
                ('ASSESS','initial_grammar_test'):(initial_grammar_test,'ASSESS','initial_grammar_report'),
                ('ASSESS','weekly_grammar_level'):(weekly_grammar_level,'ASSESS','weekly_grammar_report'),
                ('ASSESS','listening_comprehension'):(listening_comprehension,'ASSESS','choose_learning_area'),
                ('ASSESS','reading_comprehension'):(reading_comprehension,'ASSESS','choose_learning_area'),
                ('ASSESS','vocab_level'):(vocab_level,'ASSESS','choose_learning_area'),
    
                ('ASSESS','initial_grammar_report'):(initial_grammar_report,'ASSESS','choose_learning_area'),
                ('ASSESS','weekly_grammar_report'):(weekly_grammar_report,'ASSESS','choose_learning_area'),
    
                # common ending
                ('COACHING',''):(holistic_report,'COACHING','holistic_report'),            
                ('COACHING','holistic_report'):(holistic_report,'COACHING','offer_coaching_tips'), # Provide tips on how to work on this week's focuses. store these to appear in next week's REPORT phase
                ('COACHING','offer_coaching_tips'):(offer_coaching_tips,'HOMEWORK','offer_exercises'), # possibly provide resource links or drills for them to work on
                ('HOMEWORK','offer_exercises'):(offer_exercises,'HOMEWORK','review_summary'),
                ('HOMEWORK','review_summary'):(review_summary,'HOMEWORK','review_summary'),
            }

        
        anon_user = Client()
        logged_in_user = Client()
        logged_in_user.login(username='ContentCreator',password='qweqweqwe') 
        url = reverse('main:switchboard')
        request = logged_in_user.get(url)        
        sp = StudentProgress.objects.get(id=logged_in_user.session['sp_pk'])
        session = logged_in_user.session
        session['target_language'] = sp.course.target_language.title_in(sp.native_language)              
        session.save()
                
        for key in switch_lookup:
            print (key)
            (sp.phase, sp.mode) = key
            sp.save()            
            response = logged_in_user.get(url)
            self.assertIn(response.status_code, [200,302])
            #print(logged_in_user.session)
               