from django.urls import include, path
from django.contrib import admin, admindocs
from lang import views as core_views
from django.conf.urls.i18n import i18n_patterns
from main import views as main_views
from django.conf.urls.static import static
from django.conf import settings
from .views import *

urlpatterns = [
    path('tinymce/', include('tinymce.urls')),    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += i18n_patterns (
    path('main/', include('main.urls')),
    path('grammar/', include('grammar.urls')),
    path('coach/', include('coach.urls')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),    
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('new_login/', core_views.new_login, name='new_login'),
    path('accounts/logout/', core_views.logout_view, name='logout_view'),
    path('', splash_view, name='splash'),
    path('no_cookies/', no_cookies, name='no_cookies'),
)
"""
django.contrib.auth.urls creates:
    accounts/ login/ [name='login']
    accounts/ logout/ [name='logout']
    accounts/ password_change/ [name='password_change']
    accounts/ password_change/done/ [name='password_change_done']
    accounts/ password_reset/ [name='password_reset']
    accounts/ password_reset/done/ [name='password_reset_done']
    accounts/ reset/<uidb64>/<token>/ [name='password_reset_confirm']
    accounts/ reset/done/ [name='password_reset_complete']
"""    
