# Generated by Django 2.0.2 on 2018-02-07 22:37

import common.MixinClasses
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0010_auto_20180205_1847'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('native_languages', models.CharField(max_length=150)),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            bases=(common.MixinClasses.TranslatableMixin, models.Model),
        ),
        migrations.AlterField(
            model_name='profile',
            name='text',
            field=tinymce.models.HTMLField(blank=True, null=True, verbose_name='Content'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='value',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
