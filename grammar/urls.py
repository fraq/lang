from django.urls import include, path
from django.views.generic import TemplateView
from . import views

app_name = 'grammar'

urlpatterns = [
# add_content views
    path('gramconcept/', views.GramConceptList.as_view(), name='gramconcept_list'),
    path('gramconcept/create/', views.GramConceptCreate.as_view(), name='gramconcept_create'),
    path('gramconcept/update/<int:pk>/', views.GramConceptUpdate.as_view(), name='gramconcept_update'),
    path('gramconcept/delete/<int:pk>/', views.GramConceptDelete.as_view(), name='gramconcept_delete'),
    
    path('grammar/dashboard/', views.create_dashboard, name='create_dashboard'),
    path('grammar/update/<int:pk>/', views.gram_update, name='gram_update'),
    path('grammar/create/', views.gram_update, name='gram_create'),    
    path('grammar/delete/<int:pk>/', views.GrammarDelete.as_view(), name='grammar_delete'),
    path('grammar/restage/<int:pk>/', views.grammar_restage, name='grammar_restage'),
        
    path('question/new_from_grammar/<int:grammar_id>/', views.update_question, name='new_question_for_grammar'),
    path('question/new_from_sentence/<int:sentence_id>/', views.update_question, name='new_question_from_sentence'),
    path('question/new/', views.update_question, name='new_question'),
    path('question/update/<int:question_id>/', views.update_question, name='update_question'),    
    path('question/delete/<int:pk>/', views.QuestionDelete.as_view(), name='question_delete'),
    path('question/list/example_questions/<int:grammar_id>/', views.example_questions, name='example_questions'),

    path('question/ratify/', views.ratify_questions, name='ratify_questions'),
    path('question/view_ratifications/', views.view_ratifications, name='view_ratifications'),
    path('question/apply_ratification_update/<int:pk>/', views.apply_ratification_update, name='apply_ratification_update'),
    
    


# assess views    
    path('assess/initial/', views.initial_grammar_test, name='initial_grammar_test'),
    path('assess/run_initial/', views.run_initial_placement, name='run_initial_placement'),
    path('assess/weekly/schedule_qs/', views.weekly_assessment_schedule_qs, name='weekly_assessment_schedule_qs'),
    path('assess/weekly/ask_qs/', views.weekly_assessment_ask_qs, name='weekly_assessment_ask_qs'),
    path('assess/question_score/', views.question_score, name='question_score'),
    path('ajax/submit_answer/', views.ajax_submit_answer, name='ajax_submit_answer'),
    path('report/', views.weekly_assessment_report, name='weekly_assessment_report'),
]
