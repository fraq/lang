from django.test import SimpleTestCase, TestCase, RequestFactory, Client
from django.urls import reverse

class Test_Loading_of_all_views(TestCase):
    """ Check each view loads without errors """
    @classmethod
    def setUpTestData(cls):
        from django.core.management import call_command
        call_command('loaddata', 'testdata.json', verbosity=0)


class TipTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        from django.core.management import call_command
        call_command('loaddata', 'testdata.json', verbosity=0)
    
    def ztest_choose_coaching_tips_from_menu(self):
        """ from navbar select coaching tips """
        c = Client()
        c.login(username='admin',password='qweqweqwe')        
        # confirm tip list is presented
        response = c.get(reverse('main:switchboard'),follow=True)
        self.assertContains(response, 'Coaching Tips')
        response = c.get(reverse('coach:tip_list'),follow=True)
        self.assertEqual(response.status_code, 200)

    def ztest_tiplist_data(self):
        """Confirm tags for a known tip appear and info is correct"""
        c = Client()
        c.login(username='admin',password='qweqweqwe')        
        response = c.get(reverse('coach:tip_list'),follow=True)
        self.assertContains(response, 'Coaching Tips')
        
        
    def ztest_choose_create_new_tip_from_tiplist(self):
        """ starting from tiplist, confirm that clicking create loads form"""
        c = Client()
        c.login(username='ContentCreator',password='qweqweqwe')        
        response = c.get(reverse('coach:tip_list'),follow=True)
        self.assertEqual(response.status_code, 200)
        # confirm create a new tip exists
        self.assertContains(response, 'Create a new tip')
        response = c.get(reverse('coach:tip_create'),follow=True)
        self.assertEqual(response.status_code, 200)
        
    def ztest_create_new_Tip_tip_only(self):
        """ Create new tip, tip only """
        c = Client()
        c.login(username='admin',password='qweqweqwe')        
        # starting from create form
        response = c.get(reverse('coach:tip_create'),follow=True)
        self.assertEqual(response.status_code, 200)
        # enter tiptext content       

        # save, confirm needs other fields
        response = c.post("/my/form/", {'something':'something'})
        self.assertFormError(response, 'form', 'something', 'This field is required.')        
        # enter stage content
        # save again
        # confirm needs title
        # add tag markings
        # save again
        # reload
        # confirm that all entered data is presented correctly

    def zcreate_new_Tip_exercise_only(self):
        response = self.client.get(reverse('grammar:grammar_progress_feedback'),follow=True)
        # starting from create form
        # enter homeworktext content
        # save, confirm needs other fields
        # enter stage content
        # save again
        # confirm needs title
        # add tag markings
        # save again
        # reload
        # confirm that all entered data is presented correctly
        