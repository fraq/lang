from googletrans import Translator
import os

translator = Translator()

def translationFile(lan, dictionnary):

    langDict = dict.fromkeys(dictionnary.keys(),[])
    for key in dictionnary:
        langDict[key] = translator.translate(dictionnary[key], dest=lan).text

    path = os.path.join('locale',lan, 'LC_MESSAGES', 'django.po')
    file = open(path, 'r')
    poKeys = {}
    for line in file.readlines():
        if line.startswith("msgid"):
            id = line.split("\"")[1]
            if id != "":
                poKeys[id] = True
    file.close()

    result = open(path, 'a+')
    keys = poKeys.keys()
    for key in dictionnary.keys():
        if key not in keys:
            result.write("msgid \"{}\"\n".format(key))
            result.write("msgstr \"{}\"\n\n".format(langDict[key]))
    result.close()


def getPotDict():
    originalPath = os.path.join('locale', 'django.pot')
    originalFile = open(originalPath, 'r')

    linesToTranslate = originalFile.readlines()
    regex = "#. Translators : "

    regex2 = "msgid \""
    dictionnary = {}
    nb_lines = 0
    for line in linesToTranslate:
        if regex in line:
            line = line.replace(regex, "")
            line = line.replace("\"", "")
            line = line.replace("\n", "")
            linesToTranslate[nb_lines + 2] = linesToTranslate[nb_lines + 2].replace(regex2, "")
            linesToTranslate[nb_lines + 2] = linesToTranslate[nb_lines + 2].replace("\n", "")
            linesToTranslate[nb_lines + 2] = linesToTranslate[nb_lines + 2].replace("\"", "")
            dictionnary[linesToTranslate[nb_lines + 2]] = line
        nb_lines = nb_lines + 1
    return  dictionnary


potDict = getPotDict()

for language in os.listdir('locale'):
    if str(language) != 'django.pot' and str(language) != 'translationWithGoogle.py':
        translationFile(language, potDict)
