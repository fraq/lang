import os
import client

pot_file_name = 'django.pot'
po_file_name = 'django.po'
base_locale_dir = 'locale'

class OneSkyWrapper:
    def __init__(self, api_key, api_secret, project_id):
        self.project_id = project_id
        self.client = client.Client(api_key, api_secret)

    def uploadPotFile(self):
        status, result = self.client.file_upload(self.project_id,
                                                 os.path.join(base_locale_dir, pot_file_name),
                                                 'GNU_POT',
                                                 is_keeping_all_strings=False,
                                                 )
        if status != 201:
            print('Could not upload POT file to project {}'.format(self.project_id))
            exit(1)

    def getTranslations(self):
        status, result = self.client.project_languages(self.project_id)
        if status != 200:
            print('Could not get languages for project {}'.format(self.project_id))
            exit(1)
        for lang in result['data']:
            code = lang['code']
            locale = lang['locale']
            status, result = self.client.translation_export(self.project_id, locale, po_file_name)
            if status == 200:
                directory = os.path.join(base_locale_dir, code, 'LC_MESSAGES')
                if not os.path.exists(directory):
                    print('Creating directory {}'.format(directory))
                    os.makedirs(directory)
                os.rename(result['downloaded_filename'], os.path.join(directory, po_file_name))
                print('Successfully downloaded {} translation'.format(locale))
            else:
                print('Unable to downloaded {} translation'.format(locale))
