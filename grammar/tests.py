import datetime
from django.utils import timezone
from django.test import SimpleTestCase, TestCase, RequestFactory
from django.contrib.auth.models import AnonymousUser, User
from django.urls import reverse
from main.models import *
from grammar.models import *
from common import css,elo,massage

class common_Tests(SimpleTestCase):

    def test_css(self):
        css_sample1 = "1,2,3,4"
        css_sample1b = "1,2,3,4,"
        css_sample2 = ""
        css_sample2b = ","
        list_sample1 = [1,2,3,4]
        list_sample2 = []
        self.assertEqual(css.to_list(css_sample1),list_sample1)
        self.assertEqual(css.to_list(css_sample1b),list_sample1)
        self.assertEqual(css.to_list(css_sample2),list_sample2)
        self.assertEqual(css.to_list(css_sample2b),list_sample2)
        
        self.assertEqual(css_sample1,css.to_css(list_sample1))
        self.assertEqual(css_sample2,css.to_css(list_sample2))
    
        css_new = ""
        css_new = css.add(css_new,1)
        css_new = css.add(css_new,2)
        css_new = css.add_unique(css_new,3)
        css_new = css.add(css_new,4)
        css_new = css.add_unique(css_new,4)
        self.assertEqual(css_new,css_sample1)
        self.assertEqual(css.length(css_new),4)

    def test_elo(self):
        winner_start_score = 800
        winner = elo.Actor(winner_start_score, 100, elo.WINNER) 
        loser_start_score = 1200
        loser = elo.Actor(loser_start_score, 100, elo.LOSER)
        elo.determine_scores([winner,loser])
        self.assertGreater(winner.score, winner_start_score)
        self.assertLess(loser.score, loser_start_score)
        
class Test_Loading_of_all_views(TestCase):
    """ Check each view loads without errors """
    @classmethod
    def setUpTestData(cls):
        from django.core.management import call_command
        call_command('loaddata', 'testdata.json', verbosity=0)

     


class ViewTests(TestCase):

    def setUp(self):
        pass
    
    def ztest_NoRegistrations_to_Registered_for_Course(self):
        self.client.login(username='joe', password='qweqweqwe')

        # check trying to access grammar_progress_feedback redirects to signup for courses
        response = self.client.get(reverse('grammar:grammar_progress_feedback'),follow=True)
        self.assertRedirects(response,reverse('main:signup_course'))

        # check content of signup page
        self.assertContains(response, self.c1.title_in(self.l_EN))
        self.assertContains(response, self.c2.title_in(self.l_EN))
        self.assertNotContains(response, self.c3.title_in(self.l_EN))
        # sign up for Learn Turkish
        response = self.client.post(reverse('main:register_student_to_course'), {'choice': "{},{}".format(self.c1.id,self.l_EN.id)}, follow=True)
        self.assertRedirects(response,reverse('grammar:create_dashboard'),status_code=302, target_status_code=200)
        self.sp = StudentProgress.objects.first()

        # simulate assessment
        self.sp.score = 3000
        self.sp.variance = StudentProgress.VARIANCE_CUTOFF_INITIAL_PLACEMENT
        self.sp.stage = self.stage1
        self.sp.induction = sp.GRAM_ASSESS_DONE 
        self.sp.save()
        # after assessment
        # subscribed - grammar_progress_feedback shows correct output
        response = self.client.get(reverse('grammar:grammar_progress_feedback'),follow=True)
#        self.assertRedirects(response,'',status_code=302,target_status_code=200)
        self.assertContains(response, self.sen_c1_tit)
        # attempt sign-up, confirm Learn Turkish is no longer shown
        response = self.client.get(reverse('main:signup_course'))
        self.assertNotContains(response, self.sen_c1_tit)
        self.assertContains(response, self.sen_c2_tit)
        self.assertNotContains(response, self.sen_c3_tit)



    def ztest_Weekly_Assessment(self):
        self.test_NoRegistrations_to_Registered_for_Course()
        # start weekly assessment
        response = self.client.get(reverse('grammar:weekly_grammar_level'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('grammar:weekly_assessment_schedule_qs'))
#        while (response.url == reverse('grammar:weekly_assessment_schedule_qs')):
#            response = self.client.get(response.url)
#        self.assertEqual(response.url, reverse('grammar:weekly_assessment_ask_qs'))
        
        # confirm a question is posed

class AttemptTests(TestCase):
    pass
    def test_hardest_questions(self):
        # returns questions with the greatest percentage of Wrong self_assessments.
        pass
        
    
    def test_most_ambiguous_questions(self):
        # returns questions with the greatest percentage of Ambiguous self_assessments.
        pass
 
class CommentTests(TestCase):
    pass

class CourseTests(TestCase):
    pass
   
"""class EntryTests(TestCase):

#     def setUp(self):
#         t = ViewTests.setUp(self)
#         print('EntryTests.Setup called')
#         t = ViewTests.setUp(self)
#         #print (self)
        
    def test_unique_users_several(self):        
        t = ViewTests.setUp(self)
        print('EntryTests.test_unique_users_several called')
        self.assertEqual(self.e1.unique_users(), 3)
        
    def test_unique_users_none(self):
        t = ViewTests.setUp(self)
        print('EntryTests.test_unique_users_none called')
        pass

    def test_recurring_patterns(self):
        t = ViewTests.setUp(self)
        print('EntryTests.test_recurring_patterns called')
        pass
    
class GramConceptTests(TestCase):
    pass


class GrammarProgressTests(TestCase):
    pass

class GrammarTests(TestCase):
    pass

class LanguageTests(TestCase):
    pass

class QuestionTests(TestCase):
    pass

class SentenceTests(TestCase):
    pass

class StageTests(TestCase):
    pass
 
class StudentProgressTests(TestCase):
    pass
    
class StudentTests(TestCase):
    pass

"""