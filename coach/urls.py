from django.urls import include, path
from django.views.generic import TemplateView
from . import views

app_name = 'coach'

urlpatterns = [
    path('tip/create/', views.tip_create_update, name='tip_create'),
    path('tip/update/<int:id>/', views.tip_create_update, name='tip_update'),   
    path('tip/list/', views.tip_list, name='tip_list'),
    path('tip/delete/<int:pk>/', views.TipDelete.as_view(), name='tip_delete'),

    path('tag/create/', views.tag_create_update, name='tag_create'),
    path('tag/update/<int:id>/', views.tag_create_update, name='tag_update'),
    path('tag/list/', views.TagList.as_view(), name='tag_list'),
    path('tag/delete/<int:pk>/', views.TagDelete.as_view(), name='tag_delete'),
]
