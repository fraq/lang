from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from common import elo, css
from grammar.models import * #GramConcept, Stage, Grammar, Student, StudentProgress, Course, Question, GrammarProgress
from main.models import Stage, Profile
from datetime import date
from django.contrib import messages
import logging
import json
import random
from django.template.loader import render_to_string
from django.http import HttpResponse
from .main import mode_completed, mode_not_completed, get_sp_or_redirect

# many of these views should eventually move into their own dedicated apps

@login_required
def can_do_statements(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": can_do_statements started")

    if request.POST:
        if form.is_valid():
            form.save()
            return mode_completed(request)
    return mode_completed(request)

@login_required
def choose_learning_area(request):
    """ 
    redirects to one of the assessments. 
    when done, redirects to COACHING.
    
    """

    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ":"+ " choose_learning_area started")

    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    # If first call to this routine today...
    if 'assessments_todo' not in request.session:
        if Profile.objects.getval(sp,'initial_grammar_test_completed') is None:
            request.session['assessments_todo'] = 'initial_grammar_test'
        else:
            # For now, each weekly assessment consists only of a weekly grammar test
            request.session['assessments_todo'] = 'weekly_grammar_level' 
    
    # get next_mode by popping it off the list
    request.session['assessments_todo'], next_mode = css.pop(request.session['assessments_todo'])
    if next_mode == '': 
        # assessments finished    
        sp.phase = 'COACHING'    
        sp.mode = ''
    else:
        # cue up next assessment 
        sp.mode = next_mode
    sp.save() 
    return mode_not_completed(request)

@login_required
def listening_comprehension(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": listening_comprehension started")

    if request.POST:
        if form.is_valid():
            form.save()
            return mode_completed(request)
    return mode_completed(request)

@login_required
def reading_comprehension(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": reading_comprehension started")

    if request.POST:
        if form.is_valid():
            form.save()
            return mode_completed(request)
    return mode_completed(request)

@login_required
def vocab_level(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": vocab_level started")

    if request.POST:
        if form.is_valid():
            form.save()
            return mode_completed(request)
    return mode_completed(request)
