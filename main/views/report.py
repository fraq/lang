from .main import mode_completed
from django.shortcuts import render, redirect, get_object_or_404
import logging
from django import forms
from main.forms import ProfileForm, SignUpForm, CheckInForm
from main.models import Language, Course, StudentProgress, Profile, Student
from coach.models import TipEncounter
from lang.views import logout_view
from .main import mode_completed, mode_not_completed, get_sp_or_redirect, printout_session
from django.contrib.auth import authenticate, login, logout
from datetime import date, timedelta, datetime

def website_induction(request):
    log = logging.getLogger(__name__)
    log.debug("User" + request.user.username + ": website_induction started")

    if request.POST:
        return mode_completed(request)
    context = {'target_language':request.session['target_language']
        }
    
    return render(request, 'main/website_induction.html')

def check_in(request):
    log = logging.getLogger(__name__)
    log.debug("User" + request.user.username + ": check_in started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    initial = {'ReportDate': date.today(),    
                'ElapsedDays':sp.days_since_last_checkin(),
                'InputActivities': '<ul><li>Attending class...</li></ul>',
               }
    context = {
        'elapsed_days':sp.days_since_last_checkin(), # days since last check-in
        'last_checkin_date':sp.last_checkin_date(),
        'Weekly_hour_goal':Profile.objects.getval(sp=sp,entry='hour_commitment'),
        }
    if request.POST:
        form = CheckInForm(request.POST, initial=initial)
        if form.is_valid():
            form.save(request)
            # email report to coach if the user has one..
            request.session['checkin_was_today'] = True # Let's review_summary know that this is not a mid-week re-visit
            Profile.objects.create_or_update(sp=sp,entry='last_checkin',text=date.today())            
            return mode_completed(request)
    else:
        form = CheckInForm(None, initial=initial)
    
    # Blank form, or form with errors
    context['form']= form
    return render(request, 'main/check_in.html', context)

# No login required - for anonymous users, however 'profile_data' should be set in session previously
def profile_user(request):
    log = logging.getLogger(__name__)
    log.info("Started profile_user")
    request.session['checkin_was_today'] = True # Let's review_summary know that this is not a mid-week re-visit

    if 'sp_pk' in request.session:
        sp = StudentProgress.objects.get(id=request.session['sp_pk'])
        mode = 'save_to_user'
        log.debug("User " + request.user.username + " view profile_user: sp already exists")        
    else:
        if 'profile_data' not in request.session:
            log.error("profile_data not in request.session when attempting to call view profile_user. redirecting to getting_started")
            printout_session(request)
            return redirect('main:getting_started')
        sp = None
        mode = 'save_to_session'
    
    if request.method == 'POST':
        log.debug("view profile_user: form submitted")        

        form = ProfileForm (request.POST,request=request)
        if form.is_valid():
            log.debug("view profile_user: form valid - saved")
            request = form.save(request=request,mode=mode)
        else:
            log.debug("view profile_user: form invalid - re-rendering")
            return render(request, 'main/profile_new_user.html', {
                'form': form,
            })
    form = ProfileForm (None,request=request)    
    if len(form.fields) > 0:
        log.debug("view profile_user: creating a form and rendering")
        return render(request, 'main/profile_new_user.html', {
            'form': form,
        })            
    else:
        # Profiling done for today
        if sp:
            log.debug("User " + request.user.username + " view profile_user: profile_user all questions done for today, redirecting to switchboard")
            return mode_completed(request)
        else:
            log.debug("view profile_user: profile_user all questions done, redirecting to register_new_user")
            return redirect('main:register_new_user')

def getting_started(request):
    log = logging.getLogger(__name__)
    log.info("starting view getting_started")
    if request.session.test_cookie_worked():
        log.debug("view getting_started: cookies enabled verified")
        request.session.delete_test_cookie()
    else:
        log.info("view getting_started: cookies not enabled, redirecting to splash page with message")
        return redirect('no_cookies')
    if 'sp_pk' in request.session:
        log.debug("User " + request.user.username + " already logged in, view:getting_started redirecting to logout view")
        logout(request)
        return logout_view(request)
    else:
        if 'profile_data' not in request.session:
            request.session['profile_data'] = {}
            log.debug("view:getting_started : request.session.profile_data did not exist, initialised as blank")

        else:
            log.debug("view:getting_started : request.session.profile_data already exists, so not reinitialising")
            printout_session(request)
        return redirect('main:profile_user')

def register_new_user(request):
    log = logging.getLogger(__name__)
    log.debug("Started register_new_user")
    printout_session(request)

    try:
        data = request.session['profile_data']
    except:
        log.error("profile_data not in request.session when attempting to call view register_new_user, redirecting to switchboard")
        printout_session(request)
        return redirect('main:switchboard')
    
    # register a user
    if request.method == 'POST':
        log.debug("register_new_user: processing posted form")
        form = SignUpForm(request.POST)
        if form.is_valid():
            log.debug("register_new_user: creating entries")
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)

            Student.objects.create(user=request.user,native_languages=data['native_languages'])           
            # create sp for selected course
            if 'course' in data:
                [course_id,native_id] = data['course'].split(',')
                course = get_object_or_404(Course, id=course_id)
                native = get_object_or_404(Language, id=native_id) 
                sp = StudentProgress.objects.create(user=request.user, course=course, native_language=native)
                request.session['sp_pk'] = sp.id
            else:
                log.error("course not in request.session when attempting to call view register_new_user, redirecting to profile_user")
                printout_session(request)
                return redirect('main:profile_user')

            # transfer profile info from session into database                        
            for key in ProfileForm.value_fields:
                if key in data:
                    Profile.objects.create_or_update(sp=sp,entry=key,value=data[key])
                        
            for key in ProfileForm.text_fields:
                if key in data:
                    Profile.objects.create_or_update(sp=sp,entry=key,text=data[key])

            # Record today as last checkin date
            Profile.objects.create_or_update(sp=sp,entry='last_checkin',text=date.today())    
            
            sp.mode = 'register_new_user'
            sp.phase = 'REPORT'
            sp.save()
            return mode_completed(request)
        else:
            log.debug("register_new_user: invalid form")                    
    else:
        log.debug("register_new_user: rendering blank form")        
        form = SignUpForm(None)
    log.debug("register_new_user: rendering signup.html form")        
    return render(request, 'registration/signup.html', {'form': form})
