from django import forms
from .models import Tip
from main.models import Language
from django.shortcuts import get_object_or_404
import logging
from tinymce.widgets import TinyMCE
from main.views import get_sp_or_redirect 
from coach.models import Tag, TipTag
from main.models import Sentence, Language

    # auto populate native_language, author, parent, 
#    tags # dropdown, categories (source, sitution, level) each leading to a separate dropdown with a control-select... also give an any field...
        # AJAX auto-complete...    Situation:I
        # checkboxes: of upto 10 possible 10 tags 

class FollowUpHomeworkForm(forms.Form):
    pass
    # presents each tip_homework title, then followup question, and a expand toggle to read full original description
    # presents each tip_advice title, then choice to bookmark, reject

class TagForm(forms.Form):
    title = forms.CharField(required=True, label='Title', widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))
    native_language = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    parent = forms.IntegerField(required=False,widget=forms.HiddenInput())
    id = forms.IntegerField(widget=forms.HiddenInput(),required=False)

    def save(self,request):
        log = logging.getLogger(__name__)
        data = self.cleaned_data

        if self.initial['id'] is None: # creating
            # create sub objects
            sentence_parent = None
            if self.initial['parent'] is not None:
                sentence_parent = Tag.objects.filter(id=self.initial['parent']).first()
                
            title_sentence = Sentence.objects.create(
                    text = data['title'],
                    language = Language.objects.get(id=self.initial['native_language']),
                    parent = sentence_parent,
                    author = request.user
                )
            # create main object
            tag = Tag.objects.create(title = title_sentence,
                                     parent = self.initial['parent'],
                                     author = request.user,
                                     )
            log.info("User " + request.user.username + " created tag "+ str(data['title']))
        else: # updating existing tag
            tag = get_object_or_404(Tag, id=self.initial['id'])
            if self.has_changed():                
                tag.title.text = data['title']
                tag.title.save()
                tag.save()
                log.info("User " + request.user.username + " updated tag " + str(data['title']))

class TipForm(forms.Form):
    title = forms.CharField(required=True, help_text="Summary title for the advice", label='Title', widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))
    text = forms.CharField(required=False, widget=TinyMCE(mce_attrs={'cols': 20, 'required': False}), help_text="Advice for the student", label='Coaching advice')
    homework_text = forms.CharField(required=False, widget=TinyMCE(mce_attrs={'cols': 20, 'required': False}), help_text="Homework exercise for the student", label='Homework task')
    id = forms.IntegerField(widget=forms.HiddenInput(),required=False)
    parent = forms.IntegerField(widget=forms.HiddenInput(),required=False)
    language_specific = forms.BooleanField(required=False, help_text="Is this tip specific to only certain languages?") # booleanfields always need required=False else if left unchecked they are considered not filled in
    target_language = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    native_language = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    stage = forms.IntegerField(required=True, help_text="Earliest stage this tip will be available")

    def __init__(self, *args, **kwargs):         
        super(TipForm, self).__init__(*args, **kwargs)         
        self.fields['tags_INC'] = forms.MultipleChoiceField(choices=self.initial['tag_choices'],
                                                         label = 'Tags',
                                                         initial = self.initial['selected_tags_INC'], 
                                                         required=False,   
                                                         widget=forms.CheckboxSelectMultiple,                                                                 
                                                         )
        self.fields['tags_EXC'] = forms.MultipleChoiceField(choices=self.initial['tag_choices'],
                                                         label = 'Tags',
                                                         initial = self.initial['selected_tags_EXC'], 
                                                         required=False,   
                                                         widget=forms.CheckboxSelectMultiple,                                                                 
                                                         )

    def clean(self):
        cleaned_data = super().clean()
        # either text or homework_text must be filled in
        if len(cleaned_data.get("text")) == 0 and len(cleaned_data.get("homework_text")) == 0:
            self.add_error('text', 'This tip must contain at least Coaching advice or a Homework task, but both cannot be blank.')  
        
    def save(self,request):
        log = logging.getLogger(__name__)
        data = self.cleaned_data

        if self.initial['id'] is None: # creating
            # create sub-objects
            # create main object
            tip = Tip.objects.create(title = data['title'],
                                     text = data['text'],
                                     homework_text = data['homework_text'],
                                     native_language = Language.objects.get(id=self.initial['native_language']),
                                     parent = self.initial['parent'],
                                     author = request.user,
                                     language_specific = data['language_specific'],
                                     stage = data['stage'],
                                     )
            log.info("User " + request.user.username + " created tip "+ str(data['title']))
        else: # updating existing tip
            tip = get_object_or_404(Tip, id=self.initial['id'])
            if self.has_changed():
                tip.title = data['title']
                tip.text = data['text']
                tip.homework_text = data['homework_text']
                # native_language : doesn't change
                # parent : doesn't change
                # author : doesn't change, stays as original
                tip.language_specific = data['language_specific']
                tip.stage = data['stage']
                tip.save()
                log.info("User " + request.user.username + " updated tip " + str(data['title']))

        # add tags
        tip.tags.clear()
        lang_tag = Tag.objects.lang_tag(self.initial['target_language'])
        lang_tag_added = False
        for tag_id in data['tags_INC']:
            tag = Tag.objects.get(id=tag_id)
            if tag == lang_tag:
                lang_tag_added = True
            TipTag.objects.create(tip=tip, tag=tag, relationship=True)
        # To prevent errors due to a tag being ticked in both lists, we first remove all 
        # tags_INC elements from tags_EXC
        modified_tags_EXC = [item for item in data['tags_EXC'] if item not in data['tags_INC']]
        for tag_id in modified_tags_EXC:
            tag = Tag.objects.get(id=tag_id)
            if tag == lang_tag:
                lang_tag_added = True
            TipTag.objects.create(tip=tip, tag=tag, relationship=False)
                     
        if data['language_specific'] == True and not lang_tag_added:
            lang_tag = Tag.objects.lang_tag(self.initial['target_language'])
            TipTag.objects.create(tip=tip, tag=lang_tag,relationship=True)
        elif data['language_specific'] == False and not lang_tag_added:
            # If later a user de-selected language_specific, we can let the language_tag remain in whatever state
            pass                
