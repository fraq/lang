from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from common import elo, css, massage
from grammar.models import * #GramConcept, Stage, Grammar, Student, StudentProgress, Course, Question, GrammarProgress
from main.models import Stage
from datetime import date
from django.contrib import messages
import logging
import json
import random
from django.template.loader import render_to_string
from django.http import HttpResponse
from main.views.main import mode_completed, mode_not_completed, get_sp_or_redirect


@login_required
def initial_grammar_test(request):
    log = logging.getLogger(__name__)
    log.debug("User" + request.user.username + ": initial_grammar_test started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    messages.add_message(request, messages.INFO, "Since you have newly registered to this course, we will start you off with a quick initial assessment. If you can't answer any questions, just click PASS.") 

    # set initial variables  
    request.session['asked qs'] = ''
        
    return redirect('grammar:run_initial_placement')

@login_required
def run_initial_placement(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": run_initial_placement started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp_query = FocusGrammarProgress.objects.filter(sp=sp)
    if fp_query.exists():
        fp = fp_query.first()
    else:
        fp = FocusGrammarProgress.objects.create(sp=sp)
    # Stop the initial assessment once the user’s variance falls below a certain level.
    if fp.variance <= fp.VARIANCE_CUTOFF_INITIAL_PLACEMENT:
        fp.set_stage() # creates grammar progress records for each stage below current
        log.info("User" + request.user.username + " completed " + __name__ + ". Placed at stage "+ str(fp.stage))
        return mode_completed(request)
    else:
        if 'current_question' in request.session:
            # this was a page-reload, so we will not choose a new question
            question = get_object_or_404(Question, pk=request.session['current_question'])
        else:
            # choose a new question
            question = fp.select_nearest_ELO_q(css.to_list(request.session['asked qs']))
            if question is None:
                # no more questions - abort
                log.warning(request, "No more questions can be found at your level. Initial placement will discontinue.")
                fp.set_stage() # also saves
                messages.add_message(request, messages.SUCCESS, "Your initial assessment has been completed. You have reached stage " + fp.stage.title_in(sp.native_language))
                if fp.stage is None:
                    log.warning("User" + request.user.username + " Course " + fp.course + " seems to have no stages registered")     
                elif fp.stage.position > 1:
                    messages.add_message(request, messages.INFO, """Please note that the first weekly assessment you do will be 
                                                                longer than usual as we will take you quickly through previous 
                                                                stages to check for any gaps""") 
                return mode_completed(request)
            else:
                request.session['asked qs'] = css.add (request.session['asked qs'], question.id)
                request.session['current_question'] = question.id
        testprogress = 1- (fp.variance - fp.VARIANCE_CUTOFF_INITIAL_PLACEMENT)/(fp.VARIANCE_MAX-fp.VARIANCE_CUTOFF_INITIAL_PLACEMENT)
        return render(request, 'grammar/question_ask.html', {'question': question, 'testprogress':testprogress})    

@login_required
def weekly_grammar_level(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": weekly_grammar_level started")

    # Starts a weekly assessment
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    fp = FocusGrammarProgress.objects.get(sp=sp)
    request.session['test_progress_fraction_current'] = 1/fp.FOCUS_GOAL # used to calculate progress bar
    fp.refresh_variance()
    fp.focuses = ""
    fp.save()
    messages.add_message(request, messages.INFO, "Let's do a Grammar Quiz and see how you have been progressing in your learning recently.") 
    return redirect('grammar:weekly_assessment_schedule_qs')

@login_required
def weekly_assessment_schedule_qs(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": weekly_assessment_schedule_qs started")
    
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp = FocusGrammarProgress.objects.get(sp=sp)
        
    Q_SCHEDULE_GOAL = 5 # total questions to schedule at a time
    REVISION_QS = 1 # number of scheduled questions that are to be random revision
    log = logging.getLogger(__name__)      
    if css.length(fp.focuses) >= fp.FOCUS_GOAL:
        return redirect('grammar:weekly_assessment_report')

    # schedule random revision questions
    for x in range(REVISION_QS):
        if fp.scheduled_qs_count() >= Q_SCHEDULE_GOAL:
            break    
        fp.scheduled_qs = css.add_unique(fp.scheduled_qs, GrammarProgress.objects.random_revision_question_id(fp))
        fp.save()
    
    # schedule questions from not yet passing grammars 
    for gp in GrammarProgress.objects.filter(fp=fp).order_by("grammar"):
        if fp.scheduled_qs_count() >= Q_SCHEDULE_GOAL:
            break    
        # Run some checks to possibly disqualify this gp as a question provider
        if gp.passing(fp.stage.position):
            continue # this gp has been successfully passed, move on
        if gp.recently_failed():
            continue         
        # this gp is approved as a question provider!
        fp.scheduled_qs = css.add_unique(fp.scheduled_qs,gp.select_question().id)
        fp.save()

    if fp.scheduled_qs_count() > 0:
        return redirect('grammar:weekly_assessment_ask_qs')
    else:
        new_stage = fp.promote()
        if new_stage is None:
            messages.add_message(request, messages.SUCCESS, 'Congratulations - you have graduated from all available stages for this course')
            log.info("User" + request.user.username + " graduated from all stages for " + str(sp.course))
            return mode_completed(request)
        else:
            messages.add_message(request, messages.SUCCESS, 'Congratulations - you have graduated to Stage' + str(new_stage))
        return redirect('grammar:weekly_assessment_schedule_qs')

@login_required
def weekly_assessment_report(request):   
    """Adds a summary of focus grammars to messages"""         
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": weekly_assessment_report started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp = FocusGrammarProgress.objects.get(sp=sp)
            
    focuses = 0
    output = "The grammars you should focus on studying for the coming week are:<ul>"
    for gp_id in css.to_list(fp.focuses):
        gp = GrammarProgress.objects.get(id=gp_id)
        focuses += 1
        output += "<li>"+gp.grammar.full_title(gp.fp.sp.native_language)+"</li>"
    if focuses > 0:
        output +="</ul>"
        messages.add_message(request, messages.INFO, output, extra_tags='safe') # safe tag allows HTML to be outputted    
    return mode_completed(request)

@login_required
def weekly_assessment_ask_qs(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": weekly_assessment_ask_qs started")

    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp = FocusGrammarProgress.objects.get(sp=sp)

    if 'current_question' not in request.session:
        remaining_qs = css.to_list(fp.scheduled_qs)
        if len(remaining_qs) == 0:
            return redirect('grammar:weekly_assessment_schedule_qs')
        else:            
            request.session['current_question'] = remaining_qs.pop()
            fp.scheduled_qs = css.to_css(remaining_qs)
            fp.save()

    question = Question.objects.get(id=request.session['current_question'])            
    testprogress  = css.length(fp.focuses)/fp.FOCUS_GOAL + (1/fp.FOCUS_GOAL - request.session['test_progress_fraction_current'])
    return render(request, 'grammar/question_ask.html', {'question': question, 'testprogress':testprogress})    
            

@login_required
def ajax_submit_answer(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": ajax_submit_answer started")

    # we don't want to have the question answers hidden, but visible if the html is inspected. Answers should be dynamically loaded.
    if request.is_ajax() and request.POST:
        question= get_object_or_404(Question, id=request.session['current_question'])
        # look to see if a similar entry has been recorded previously
        entry_simplified = massage.simplify(
            request.POST['user_entry'],
            native=question.native_language.iso693_2,
            target=question.back.language.iso693_2,
        )
        # determine correctness of entered answer against answer recorded in DB
        is_db_answer = massage.compare(request.POST['user_entry'],
                           question.back.text,
                           native=question.native_language.iso693_2,
                           target=question.back.language.iso693_2)
        
        matching_entries = Entry.objects.filter(question=question,simplified=entry_simplified).all()
        if matching_entries.count() == 0:
            # no similar entry found, creating a new entry
            entry = Entry.objects.create(
                text=request.POST['user_entry'],
                simplified=entry_simplified,
                question=question,
                evaluation = Entry.CORRECT if is_db_answer else Entry.UNSCORED,
            )
        else:
            entry = matching_entries.first()
            entry.increase() # increase count of how many times this entry has been encountered
            if matching_entries.count() > 1:                
                log = logging.getLogger(__name__)
                log.warning("User" + request.user.username + " Multiple Entries for same simplified value: Q.id:"+str(question.id)+" simplified:"+str(simplified))        
        # check if answer matches question's recorded answer
        (eval,confident) = entry.score()
        examiner_comment = {(Entry.CORRECT,True):"That answer is correct",
                            (Entry.WRONG,True):"That answer is incorrect",
                            (Entry.ALTERNATIVE,True):"", #"That answer is different to what was desired, but a valid alternative",
                            (Entry.UNSCORED,False):"", #"I'm not sure if that answer is correct or not, what do you think?",
                            (Entry.CORRECT,False):"That answer is probably correct. What do you think?",
                            (Entry.WRONG,False):"That answer may be incorrect. What do you think?",
                            (Entry.REPORT,False):"There may be a problem with this question. What do you think?",
                            }
        request.session['entry_id']=entry.id
        request.session['raw_entry']=request.POST['user_entry']
        # give user a chance to self-evaluate
        data = render_to_string('grammar/post_answer_submit.html', 
                                {'question': question,
                                 'examiner_comment': examiner_comment[eval,confident],
                                 'examiner_result': eval,
                                 'evaluation_confident': confident,
                                 'Correct':Attempt.CORRECT, 
                                 'Wrong':Attempt.WRONG, 
                                 'Report':Attempt.REPORT,
                                 },
                                 request=request) 
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404

@login_required
def question_score(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": question_score started")
    
    # score the question_result. store results
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp = FocusGrammarProgress.objects.get(sp=sp)
    
    question = get_object_or_404(Question, pk=request.session['current_question'])

    if 'raw_entry' not in request.session: # Pass button was pressed
        pass
        entry_eval = Entry.WRONG
        confident = True
    else: 
        entry=Entry.objects.get(id=request.session['entry_id'])
        # create an attempt pointing to entry
        Attempt.objects.create(user=request.user,
                               raw_entry=request.session['raw_entry'],
                               entry=entry,
                               assessment=request.POST['self_assessment'],
                               feedback=request.POST['feedback'],
                               score=fp.score,
                               variance=fp.variance,
                               )
        del request.session['raw_entry'] # clears it from session as it is used to determine if pass button was pressed
        self_assessment = request.POST['self_assessment']
        (entry_eval,confident) = entry.score()
            
    grammar = question.grammar
    gp = GrammarProgress.objects.filter(fp=fp, grammar=grammar).first()
    if gp is None:
        gp = GrammarProgress.objects.create(fp=fp, grammar=grammar)
    
    # calc new ELOs
    if entry_eval in [Entry.UNSCORED, Entry.REPORT]:
        entry_eval = self_assessment

    if entry_eval == Entry.CORRECT:
        q_position = elo.LOSER
        s_position = elo.WINNER
        fp.variance = fp.variance * 0.95        
    elif entry_eval == Entry.REPORT:
        q_position = elo.DRAW
        s_position = elo.DRAW
        fp.variance = fp.variance * 0.9
    else: #Entry.WRONG
        q_position = elo.WINNER
        s_position = elo.LOSER
        fp.variance = fp.variance * 0.8

    q_variance = question.variance / fp.variance # the greater the student's variance, the less impacted the question should be by that student's result    
    q_actor = elo.Actor(question.score, q_variance, q_position)
    s_actor = elo.Actor(fp.score, fp.variance, s_position)
    elo.determine_scores([q_actor,s_actor])
    gp.add_score(entry_eval)
    fp.rescore(s_actor.score)
    
    # clear current question - allows a new question to be posed
    del request.session['current_question']
    
    if sp.mode == 'weekly_grammar_level':
        if gp.recently_failed():
            # add as a focus
            fp.focuses = css.add_unique(fp.focuses,gp.id)
            fp.save()
            request.session['test_progress_fraction_current'] = 1/fp.FOCUS_GOAL
        else:
            request.session['test_progress_fraction_current'] *= 0.9 
        question.rescore(q_actor.score)
        if css.length(fp.scheduled_qs) == 0:
            return redirect('grammar:weekly_assessment_schedule_qs')
        else:   
            return redirect('grammar:weekly_assessment_ask_qs')
    elif sp.mode  == 'initial_grammar_test':  
        gp.delete()
        return redirect('grammar:run_initial_placement')

@login_required
def grammar_progress(request, context):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ":"+ __name__ + " grammar_progress started")
    context.update(grammar_progress_table_data (request))
    
    return render(request, 'grammar/grammar_progress.html', context)

@login_required
def grammar_progress_table_data(request):    
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp = FocusGrammarProgress.objects.get(sp=sp)

    # determine progress in relevant stages
    stage_array={}
    relevant_stages = Stage.objects.filter(course=sp.course,position__lte=fp.stage.position)
        
    counter = 0
    for stg in relevant_stages:
        stage_array[counter]={'type':'stage','stage_titles':stg.title_in(sp.native_language)}
        relevant_grammars = stg.grammar_set.all() #.order_by('score','title')
        counter += 1
        for grm in relevant_grammars:
            highlight = False
            gp = GrammarProgress.objects.filter(fp=fp, grammar= grm).first()
            if gp is None: # this shouldn't happen
                grammar_progress = 0
                id = 0
                colour = "Yellow" # untested
            elif gp.started == False:
                grammar_progress = 0
                id = gp.id
                colour = "Yellow" # untested
            else:
                if gp.id in css.to_list(fp.focuses):
                    highlight = True
                grammar_progress = gp.get_score()
                id = gp.id
                if gp.passing(fp.stage.position):
                    colour = "Green" # passing
                else:
                    colour = "Blue" # not passing

            stage_array[counter]={'type':'grammar',
                                  'grammar_progress':grammar_progress,
                                  'grammar_titles':grm.full_title(sp.native_language),
                                  'id':id,
                                  'explanation':grm.explanation_in(sp.native_language),
                                  'modal_title': grm.title_in(sp.native_language),
                                  'colour':colour, 
                                  'highlight':highlight}
            counter += 1
    context = {
        'stage_array':stage_array,
        'relevant_stages':relevant_stages,
        'course_title': sp.course.title_in(sp.native_language),
        'grammar_stage': fp.stage.title_in(sp.native_language),
    }
    return context
