from django.template.defaulttags import register


# a custom filter for templates. permits 2 level arrays
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def index(List, i):
    return List[int(i)]

@register.filter
def entry_num_array(List):
    return range(len(List))
