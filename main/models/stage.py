from django.db import models
from django.contrib.auth.models import User
from common.MixinClasses import TranslatableMixin
import logging

# Stages can provide an ordering to grammars, as well as milestones to mark a student's progress. 

class StageManager(models.Manager):
    def determine_stage(self,course,score):
        """
        Given an ELO score, returns the stage most appropriate (the lowest stage with a score higher than **score**) 
        If the score is above all available stages, returns last registered stage
        """
        return self.stage_above(course,score)

    def stage_above(self,course,score):
        """Returns lowest stage with a score above **score**"""
        last_stage = None
        for stage in Stage.objects.filter(course=course).order_by("position"):
            last_stage = stage
            if stage.score() > score:
                break
        return last_stage
        
    def stage_below(self,course,score):
        """Returns highest stage with a score below **score**"""
        last_stage = None
        for stage in Stage.objects.filter(course=course).order_by("position"):
            if stage.score() > score:
                break
            last_stage = stage                
        return last_stage

class Stage(models.Model, TranslatableMixin):
    position = models.IntegerField()
    title = models.ForeignKey('Sentence', null=True, on_delete=models.SET_NULL, related_name='StageTitle')
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    last_edited = models.DateField(auto_now=True)
    # to access stage's grammars, stage.grammar_set
    objects = StageManager()

    def __str__(self):
        return str(str(self.position) + ". " + TranslatableMixin.__str__(self))

    def title_in(self,language):
        return str(str(self.position) + ". " + TranslatableMixin.title_in(self,language))

    def __init__(self, *args, **kwargs):
        super(Stage, self).__init__(*args, **kwargs)

    def score(self):
        return self.position*1000

    def save(self, *args, **kwargs):
        super(Stage, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Stage Instance saved:"+str(self))
        
    class Meta:
        ordering = ['position'] # lowest scores first
        unique_together = ("position", "course")
