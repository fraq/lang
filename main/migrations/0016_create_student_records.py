# Custom Migration
# With the introduction of student model, existing 
# users won't have a student record that will cause a crash

from django.db import migrations, models
import logging
from main.models import StudentProgress, Student, Profile

def create_student_records(apps, schema_editor): 
    log = logging.getLogger(__name__)
    log.info("create_student_records migration started")

    # create a student record for each user
    for sp in StudentProgress.objects.all():
        if not Student.objects.filter(user=sp.user).exists():
            Student.objects.create(user=sp.user,native_languages=Profile.objects.getval(sp,'native_languages'))
    log.info("create_student_records migration ended")

class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_data'),
    ]

    operations = [
        migrations.RunPython(create_student_records),
    ]
