from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count
import logging

class Entry(models.Model):
    """
    Entries track similar `:model:grammar.attempt` s 
    # records question, sentence, what was typed, count of times occurred, evaluation, 
    # evaluation strength (how many qualified people have evaluated it thus)
    # does not record user, these are recorded in Attempt model
    """
    
    CORRECT = 'C'
    WRONG = 'W'
    REPORT = 'R'
    ALTERNATIVE = 'A'
    UNSCORED = 'U'
    E_TYPE_CHOICES = ((CORRECT, 'Answer is correct'), (WRONG, 'Answer is incorrect'), (REPORT, 'Question is problematic'), (ALTERNATIVE, 'Answer is different to desired, but a valid alternative'), (UNSCORED, 'Not yet evaluated'))   

    text = models.CharField(max_length=200, help_text="entry text")
    simplified = models.CharField(max_length=200, help_text="massaged entry text", db_index=True) # internal hashprocessing to make comparing similar entries easier
    question = models.ForeignKey('Question', db_index=True, on_delete=models.CASCADE)
    #entry.question.back = the ideal answer
    last_edited = models.DateField(auto_now=True)
    evaluation = models.CharField(max_length=1,choices=E_TYPE_CHOICES, default = UNSCORED, db_index=True) 
    # Reason is a many-to-many field to permit reasons to be provided in multiple native languages
    reasons = models.ManyToManyField('main.Comment',blank=True,help_text="Reasons provided by assessors to why this question is evaluated thus")
    count = models.IntegerField(default=1,help_text="How many times unique users have made this entry")
    
    def __str__(self):
        return str([self.question, self.text, self.simplified, self.evaluation])

    def increase(self):
        # register another user who answered with this entry
        self.count += 1
        self.save()

    def save(self, *args, **kwargs):
        super(Entry, self).save(*args, **kwargs)

    def unique_users(self): 
        # Number of unique users who have entered this answer
        all_attempts = self.attempt_set.all() # All attempts for this entry
        # now count the number of unique users
        return all_attempts.aggregate(Count('user',distinct=True))['user__count']
    
    def score(self):
        # returns a ranking based on past entries. If the entry has not yet been scored, gives a score based
        # on past user assessments. Second parameter returned indicates if the score is certain, or 
        # a guess based on other user self-assessments.
        if self.evaluation in [self.CORRECT, self.WRONG, self.ALTERNATIVE]:
            return (self.evaluation,True)
        elif self.simplified == self.question.simplified:
            self.evaluation = self.CORRECT
            self.save()
            return (self.evaluation,True)
        elif len(self.simplified) == 0:
            self.evaluation = self.WRONG
            self.save()
            return (self.evaluation,True)             
        else:
            all_attempts = self.attempt_set.values('user','assessment').distinct() # All attempts for this entry, one per user
            count = {self.CORRECT:0,self.WRONG:0,self.REPORT:0}
            for attempt in all_attempts:
                count[attempt['assessment']] += 1
            total = count[self.CORRECT] + count[self.WRONG] + count[self.REPORT]
            
            if total == 0:
                return (self.UNSCORED,False)
            # attempt entries exist
            elif count[self.CORRECT] > 0:
                if count[self.CORRECT] == total:
                    return (self.CORRECT,False)
                else:
                    return (self.UNSCORED,False) # uncertain
            # no correct self-assessments recorded
            else:
                if count[self.WRONG] > count[self.REPORT]:
                    return (self.WRONG,False)
                else:
                    return (self.REPORT,False)

    class Meta:
        verbose_name_plural = "Entries"
        ordering = ['-last_edited']
#         order_with_respect_to = 'question'
        unique_together = ("question", "text")
        