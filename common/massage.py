mapping_dict = {}
mapping_dict['tur'] = {'ı':'i','İ':'I','ğ':'g','ç':'c','ş':'s','ö':'o','ü':'u'}
ignore_symbols = " ',.!?;:"

def compare(input1,input2,native='eng',target='tur'):
    return simplify(input1,native=native,target=target) == simplify(input2,native=native,target=target)  

def simplify(input,native='eng',target='tur'):
    # native and target are iso693_2 codes
    # see https://www.loc.gov/standards/iso639-2/php/code_list.php
    # native is the native language of the user
    # target is the target language the user is writing in. Note: This may be
    #   the target language they are learning, but as some questions ask the
    #   student to translate from the target->native, the target language here
    #   might be equal to their native language.

    if native.lower() in ("eng"):
        in_mapping='eng'
        if target.lower() in ("tur"):
            out_mapping='tur'        
            return map_string(input,in_mapping='eng',out_mapping='tur')
        else:
            return map_string(input)
    else:
        return map_string(input)

def map_string(input,in_mapping='eng',out_mapping='tur'):
    simplified_string=''    
    for character in input:
        char = character.lower() 
        if char not in ignore_symbols:
            if char in mapping_dict[out_mapping]:
                simplified_string += mapping_dict[out_mapping][char]
            else:
                simplified_string += char    
    return simplified_string
    