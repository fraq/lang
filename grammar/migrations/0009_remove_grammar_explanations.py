# Generated by Django 2.0.1 on 2018-01-09 12:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('grammar', '0008_auto_20180109_1457'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='grammar',
            name='explanations',
        ),
    ]
