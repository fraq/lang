FROM python:3

WORKDIR /usr/src/app

# Setup dependencies
COPY requirements.txt ./
RUN pip install -r requirements.txt
RUN apt-get update && apt-get install -y gettext

# Install and run app
COPY app/ .
# Entrypoints allow for arguments to wsgi.py to be appended
# Change this to CMD if you just want an all-or-nothing

CMD python manage.py runserver 0.0.0.0:8000
