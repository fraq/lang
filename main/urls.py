from django.urls import include, path
from . import views

app_name = 'main'

urlpatterns = [
    path('test/', views.test, name='test'), 
    path('integrity_check/', views.integrity_check, name='integrity_check'), # run some sort of test - for debugging
    path('', views.switchboard, name='switchboard'),
    path('switchboard/<mode>/', views.switchboard, name='switchboard'),
    path('review_summary/', views.review_summary, name='review_summary'),
                  
    path('select_course/', views.select_course, name='select_course'),
    path('signup_course/', views.signup_course, name='signup_course'),
    path('reset_all_student_progress/', views.reset_all, name='reset_all'),
    path('add_stage/', views.add_stage, name='add_stage'),
    path('getting_started/', views.getting_started, name='getting_started'),
    path('profile/', views.profile_user, name='profile_user'),
    path('register_new_user/', views.register_new_user, name='register_new_user'),
    
]
