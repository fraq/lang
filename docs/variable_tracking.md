========================================================
==cascade flowchart showing which functions call which==
========================================================

weekly_grammar_level
    => weekly_assessment_schedule_qs
        => weekly_assessment_report
            => adds a message listing grammars to focus on
                => mode_completes                
                    => weekly_grammar_report
                    
        OR
        => weekly_assessment_ask_qs
            => question_ask.html
                => ajax_submit_answer
                    => post_answer_submit
                        => question_score
                            => weekly_assessment_schedule_qs, weekly_assessment_ask_qs
                            
initial_grammar_test
    => run_initial_placement
        => question_ask.html
                => ajax_submit_answer
                    => post_answer_submit
                        => question_score
                            => run_initial_placement
        OR
        => adds a message declaring what stage the user is placed at
            => mode_completes
                => initial_grammar_report
                    => adds message, initial assessment completed
                        => grammar_progress
                            => grammar_progress_table_data (loaded into context)
                            => grammar_progress.html
                                => mode_completes 
                                    => choose_learning_area

review_summary
    => grammar_progress_table_data (loaded into context)
    => review_summary.html

    
=================================    
==request.session keys and uses==
=================================
	
	sp_pk: StudentProgress field currently loaded. Identifies if user is logged into a course
		select_course
	
	selected_stages: used by question creation, which stage tabs are currently selected
		update_question: reads,writes
	
	mode: 'initial placement','weekly assessment'
		initial_placement_init
		weekly_grammar_level
		question_score
	
	asked_qs: tracks qs asked so far to avoid duplicates in initial assessment <-- also use in weekly assessment?
		initial_placement_init
		run_initial_placement

	test_progress_fraction_current: used to calculate progress bar
		weekly_grammar_level
		weekly_assessment_ask_qs
		question_score
		
	current_question: prevents a user reloading the page and avoiding an unwanted question
		weekly_assessment_ask_qs
		run_initial_placement
		question_score <-- clears it to allow a new question to be posed

	question_id: records question_id that matches user answer
		ajax_submit_answer
		
	entry_id: records entry that matches user answer
		ajax_submit_answer
		question_score
	
	raw_entry: records raw_entry that matches user answer
		ajax_submit_answer
		question_score
		
	
	course_choice: temporary, to forward course choice to view
		dashboard
		select_course
		register_student_to_course
	
	course_title:
		select_course
		