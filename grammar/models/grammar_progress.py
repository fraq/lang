from django.db import models
from django.contrib.auth.models import User
from .question import Question
from .attempt import Attempt
from django.core.validators import validate_comma_separated_integer_list
import collections
from common import css
from datetime import date
import logging
import random

# Intermediate table between Grammar and FocusGrammarProgress
# Multiple GrammarProgressions and FocusGrammarProgress sum up a student's progress through
# a language learning course. While a student has an overall score recorded in FocusGrammarProgress
# they start each grammar with a zero score that must be built up. The score is based on the average
# of the user's past answers. A history of up to the last 20 attempts are remembered. Past_results
# begins with two Wrongs recorded so that two Rights are required to score 50% and 20 Rights to score 100%

class GrammarProgressManager(models.Manager):
    def create_gp(self, fp, grammar, stage_position):
		# creates a gp with modified score to allow quick passing if it is of a lower level than the current
        gp = self.create(fp=fp,grammar=grammar)
        gap = stage_position - grammar.stage.position 
        if gap < 1:
        	passes_needed = 2
        elif gap < 4:
        	passes_needed = 1
        else:
        	passes_needed = 0	
        gp.score = gp.pass_score(stage_position) - passes_needed 
        gp.save()
        return gp

    def random_revision_question_id(self, fp):
        """selects a random question from existing GrammarProgress, returns None if all GPs have grammars with no valid questions"""
        valid_gps = GrammarProgress.objects.filter(fp=fp).order_by("grammar").values_list('id', flat=True)
        while valid_gps:
            gp_id = random.choice (valid_gps)
            gp = self.get(id=gp_id)
            question = gp.select_question()
            if question:
                return question.id 
            else:
                valid_gps.remove(gp_id)
        return None

class GrammarProgress(models.Model):    
    START_SCORE = 5
    MAX_SCORE = 10
    
    fp = models.ForeignKey('FocusGrammarProgress', on_delete=models.CASCADE, db_index=True)
    grammar = models.ForeignKey('Grammar', on_delete=models.CASCADE, db_index=True)
    asked_qs = models.TextField(blank=True,null=True,validators=[validate_comma_separated_integer_list],default="")
    score = models.IntegerField(default=START_SCORE)
    last_attempt_date = models.DateField(auto_now=True)
    started = models.BooleanField(default=False) # set true when add_q is run
    recent_error_count = models.IntegerField(default=0)
    objects = GrammarProgressManager()

    def pass_score(self, user_stage_position):
        if self.grammar.question_set.exists():
            gap = user_stage_position - self.grammar.stage.position 
            if gap < 0:
            	return self.START_SCORE + 2
            elif gap < 2:
            	return self.START_SCORE + 3
            elif gap < 4:
            	return self.START_SCORE + 4
            else:
            	return self.START_SCORE + 5			
        else:
        	return 0
	
    def passing(self,user_stage_position):
        # reports if the user presently holds a passing score for this grammar
        return self.score > self.pass_score(user_stage_position)
        
    def add_q(self,question):
        # records that this question has been previously asked
        self.asked_qs = css.add(self.asked_qs,question.id)
        self.started = True
        self.save()
    
    def recently_failed(self):
        # reports if there have been two or more fails at this grammar in the last 24-48 hours
        days_elapsed = (date.today() - self.last_attempt_date).days
        return (days_elapsed <= 1 and self.recent_error_count >= 2)
        
    def get_asked(self):
        # returns a list of tuples sorted from least common asked question to most common (for asked qs)
        asked_qs = css.to_list(self.asked_qs)
        frequency_counted_qs = collections.Counter(asked_qs)
        try:
            return (list(frequency_counted_qs.keys()), frequency_counted_qs.most_common()[-1][0])
        except:
            return ([], None)
        
    def get_unasked(self):
        # returns all questions that have never been asked for this grammar
        return Question.objects.filter(grammar=self.grammar).exclude(id__in=self.get_asked())

    def select_question(self):
        # provides question for this grammar never asked, otherwise least asked
        possible_qs = self.grammar.question_set.all()
        asked_qs,least_asked = self.get_asked()
        unasked_qs = possible_qs.exclude(id__in=asked_qs)
        if unasked_qs.exists():
            selected_q = unasked_qs.best_quality()
            self.add_q(selected_q)
        elif least_asked is not None:
            selected_q = Question.objects.get(id=least_asked)
            self.add_q(selected_q)
        else: # no questions registered for this grammar
            return None
        return selected_q

    def add_score(self,score):
        # Adds the latest attempt score to the model record
        days_elapsed = (date.today() - self.last_attempt_date).days
        if days_elapsed > 1:
        	self.recent_error_count = 0
        if score == Attempt.WRONG:
        	self.score -= 1
        	self.score = max (self.score,0) # score cannot go negative
        	self.recent_error_count += 1
        elif score == Attempt.CORRECT:
        	self.score += 1 
        self.save()        
        return self.recent_error_count

    def get_score(self):
        return self.score/self.MAX_SCORE

    def __str__(self):
        return str([self.fp.sp.user, self.grammar, str(self.get_score()*100)+"%"])


    def save(self, *args, **kwargs):
        super(GrammarProgress, self).save(*args, **kwargs)
     
    class Meta:
        verbose_name_plural = "Grammar Progressions"
        unique_together = ("fp", "grammar")
        order_with_respect_to = "grammar"