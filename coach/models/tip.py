from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView 
from django.db import models
from django.contrib.auth.models import User
import logging
from main.models import Language
from coach.models import TipEncounter
from tinymce import HTMLField

class TipManager(models.Manager):
    def select(self, sp, total_point=12):
        """
            Returns a list of Tips selected for viewing by the user.
            total_points should be set in the range 10-26 
        """
        encountered_tip_ids = TipEncounter.objects.filter(sp=sp).values_list('tip',flat=True)
        if encountered_tip_ids is None:
            encountered_tip_ids = []
        candidate_tips = Tip.objects.exclude(id__in=encountered_tip_ids).filter(stage__lte=sp.stage.position).order_by('stage')
        tiny_count = 0
        medium_count = 0
        large_count = 0
        selected = []
        
        
        # pick tips
        remaining_points = total_point
        for choice in candidate_tips:
            # check if tags match user profile
            # check size fits
            if choice.size <= remaining_points:
                if choice.size == Tip.TINY:
                    if tiny_count > 2:
                        continue
                    else:
                        tiny_count += 1
                        
                if choice.size == Tip.MEDIUM:
                    if medium_count > 1:
                        continue
                    else:
                        medium_count += 1
                if choice.size == Tip.LARGE:
                    if large_count > 0:
                        continue
                    else:
                        large_count += 1
                remaining_points -= choice.size                        
                selected.append(choice)
                TipEncounter.objects.create(tip=choice,sp=sp)
                remaining_points -= choice.size
        return selected

class Tip(models.Model):
    """
    """

    TINY = 2
    MEDIUM = 5
    LARGE = 10
    SIZE_CHOICES = ((TINY, 'Tiny'), (MEDIUM, 'Medium'), (LARGE, 'Large'))   

    title = models.CharField(max_length=80)
    text = HTMLField(blank=True,null=True)
    homework_text = HTMLField(blank=True,null=True)

    # Unlike other models, tips have native_language and parent recorded as language translations often involve specialized alteration
    native_language = models.ForeignKey('main.Language', on_delete=models.CASCADE)
    parent = models.ForeignKey('Tip', null=True, blank=True, on_delete=models.SET_NULL, db_index=True, help_text="If this tip is a translation of another tip")
    tags = models.ManyToManyField('Tag', through='TipTag')
    # USE TAGS AND BAYSIAN... target_language = models.ManyToManyField('main.Language', blank=True, help_text="Null if this tip is suitable to multiple languages")
    bookmarks = models.IntegerField(default=0, help_text="Number of people who bookmarked this tip")
    exposures = models.IntegerField(default=0, help_text="Number of people who were presented this tip")
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    language_specific = models.BooleanField(default=False)
    stage = models.IntegerField(help_text="Earliest stage this tip will be available")
    size = models.IntegerField(choices=SIZE_CHOICES, db_index=True, default=MEDIUM, help_text="How much work and reading this tip requires")   
    objects = TipManager()

    def uptake(self):
        if self.exposures > 0:
            return self.bookmarks/self.exposures
        else:
            return 0

    def save(self, *args, **kwargs):
        # work out size of tip
        # If has homework, automatically LARGE.
        # If text section of text has a len>100 size = MEDIUM
        # If text < 100, size = TINY
        if self.homework_text is not None:
            if len(self.homework_text) > 20:
                self.size = self.LARGE
        elif self.text is not None: 
            if len(self.text) > 100:
                self.size = self.MEDIUM
        else:
            self.size = self.TINY
        super(Tip, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.title)
    
    class Meta:
        ordering = ['-created']

