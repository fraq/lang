from django.db import models
from django.contrib.auth.models import User
from django.db.models import Avg, Q
from main.models import Stage, Comment
from common.MixinClasses import TranslatableMixin

import logging
    
""" 
    Grammar is an instance of a GrammarConcept specific to a particular target_language
    (there may be multiple Grammars expressing a specific GrammarConcept in that target_language)

    explanations provide teaching explanations of that Grammar for different native_languages
    example_questions illustrating the grammar for different native_languages. 
    score reflects the difficulty of the grammar in relation to others for its stage, it also
    provides a starting score for all newly added questions indexed to this grammar. 
"""
class GrammarManager(models.Manager):
    def nearest_grammars(self,course,score):
        # returns all grammars for stage on either side of score
        lower_stage = Stage.objects.stage_below(course,score)
        upper_stage = Stage.objects.stage_above(course,score)
        return super(GrammarManager, self).get_queryset().filter(Q(stage=lower_stage)|Q(stage=upper_stage))

    def unassigned_grammars(self,course):
        """Returns any grammars not currently assigned to a stage of this course. Useful for maintenance check"""
        return super(GrammarManager, self).get_queryset().filter(target_language=course.target_language).exclude(stage__course=course)

class Grammar(TranslatableMixin, models.Model):
    stage = models.ForeignKey('main.Stage', db_index=True, null=True, on_delete=models.SET_NULL)
    gram_concept = models.ForeignKey('GramConcept', null=True, default=None, blank=True, help_text="Each grammar in a language is an example of a non-language specific grammar concept", on_delete=models.SET_NULL)
    target_language = models.ForeignKey('main.Language', related_name='target_grammars', help_text="The language this grammar belongs to", on_delete=models.CASCADE)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    title = models.ForeignKey('main.Sentence', on_delete=models.PROTECT, related_name='GrammarTitle')
    explanation = models.ForeignKey('main.Comment',null=True,blank=True,on_delete=models.SET_NULL, related_name='GrammarExplanation',help_text="Teaching explanation on this grammar written in a variety of user native languages") 
    example_questions = models.ManyToManyField('Question', blank=True,related_name='grammar_example_questions', help_text="A question that is a good representative example of this grammar")
    score = models.IntegerField(null=True,blank=True,db_index=True,help_text="Grammars have an ELO score that increases further as you work through a stage, hence indicating order")
    old_score = models.IntegerField(blank=True, default=0)
    last_edited = models.DateField(auto_now=True)
    objects = GrammarManager()

    def full_title(self,language):
        return (self.title_in(language) + "  (" + self.gram_concept.title_in(language) + ")")

    def explanation_in(self,language):        
        explanation = Comment.objects.locate_in(self.explanation,language)
        if explanation is None:
            return ''
        else:
            return explanation.text

    def save(self, *args, **kwargs):
        # Primarily this is to be called when the instance is initially created. setting the inital score value.
        if self.score is None:
            if self.stage is not None:
                self.score = self.stage.score()
                self.old_score = self.score
        elif self.score != self.old_score:
            old_score = self.old_score
            self.old_score = self.score
            for question in self.question_set.all():
                # modify question scores - work out average of scores resave score with offset.
                offset = question.score - old_score
                question.score = self.score + offset
                question.save()
        super(Grammar, self).save(*args, **kwargs)

    def rescore(self):
        # rescores the grammar to be average of its question ELOs
        old_score = self.score
        self.score = Question.objects.filter(grammar = self.id).aggregate(Avg('score'))         # get average score of all questions
        self.save()        
        log = logging.getLogger(__name__)
        log.info("Model Grammar rescored from "+old_score+"->"+self.score)

    class Meta:
        ordering = ['score']
        permissions = (
            ("content_creator", "Can create,read,update,delete"),
            ("super_creator", "Can see all setable fields"),
        )
