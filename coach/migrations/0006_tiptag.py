# Generated by Django 2.0.1 on 2018-01-08 15:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('coach', '0005_tip_stage'),
    ]

    operations = [
        migrations.CreateModel(
            name='TipTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('relationship', models.BooleanField(db_index=True, default=True, help_text='True if Tag description must be true, False if Tag description must be false')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='coach.Tag')),
                ('tip', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='coach.Tip')),
            ],
        ),
    ]
