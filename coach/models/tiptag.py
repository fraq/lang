from django.db import models

class TipTag(models.Model):
    tip = models.ForeignKey('Tip', on_delete=models.CASCADE)
    tag = models.ForeignKey('Tag', on_delete=models.CASCADE)
    relationship = models.BooleanField(default = True, db_index=True, help_text="True if Tag condition required, False if Tag condition rejected/must be false") 
