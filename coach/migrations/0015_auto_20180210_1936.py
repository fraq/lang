# Generated by Django 2.0.2 on 2018-02-10 16:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coach', '0014_auto_20180210_1936'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tip',
            old_name='homework_text2',
            new_name='homework_text',
        ),
    ]
