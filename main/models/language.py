from django.db import models
from django.contrib.auth.models import User
from common.MixinClasses import TranslatableMixin
import logging

# Languages are used to populate native_language and target_language fields
# Each language has a name, written in the script of that language, and an
# anglicized_name expressing the English name for that language.

class LanguageManager(models.Manager):
    def English(self):
        return Language.objects.get(id=Language.ENGLISH)

class Language(TranslatableMixin, models.Model):
    ENGLISH = 1 # id for English should always be 1
    
    title = models.ForeignKey('main.Sentence', on_delete=models.PROTECT, related_name='LanguageTitle')
    iso693_2 = models.CharField(max_length=8, help_text="Please find the correct code at: https://www.loc.gov/standards/iso639-2/php/code_list.php") 
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(auto_now_add=True)
    objects = LanguageManager()

    def save(self, *args, **kwargs):
        super(Language, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Language Instance saved:"+str(self))

    class Meta:
        pass