from django.db import models
from django.utils.translation import ugettext_lazy as _
from tinymce import HTMLField
from common import css
import logging

class ProfileManager(models.Manager):
    def getval(self,sp,entry):
        # Returns None if entry not found
        query = Profile.objects.filter(sp=sp,entry=entry)
        if query.exists():
            record = query.first()
            if record.text is not None:
                return record.text
            else:
                return css.literal(record.value)
        else:
            return None
    
    def create_or_update(self,sp,entry,value=None,text=None):
        query = Profile.objects.filter(sp=sp,entry=entry)
        if query.exists():
            record = query.first()
        else:
            record = Profile.objects.create(sp=sp,entry=entry)
        if value is not None:
            record.value=value
        else:
            record.text=text
        record.save()

class Profile(models.Model):
    """
        Stores entry/value pairs for students of a course to be a flexible storage of data on the student
    """
    sp = models.ForeignKey('main.Studentprogress', on_delete=models.CASCADE)
    entry = models.CharField(max_length=50)
    value = models.CharField(null=True, max_length=80,blank=True)
    text = HTMLField('Content',null=True,blank=True) # for longer user answers
    date = models.DateField(auto_now=True)
    objects = ProfileManager()

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Profile Instance saved:"+str(self))

    def __str__(self):
        return  str([self.sp.id, self.entry])
        
    class Meta:
        verbose_name_plural = "Profiles"
        ordering = ['date']
        unique_together = ("sp", "entry")
    