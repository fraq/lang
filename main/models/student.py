from django.db import models
from django.contrib.auth.models import User
from .language import Language
from .stage import Stage
from django.utils.translation import ugettext_lazy as _
from common.MixinClasses import TranslatableMixin
from common.css import literal
import logging

class Student(TranslatableMixin, models.Model):
    """
    Values for a user that are independent of paricular course they are studying
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    native_languages= models.CharField(max_length=150,null=True)

    def list_native_languages(self):
        return literal(self.native_languages)
        
    def save(self, *args, **kwargs):
        super(Student, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Student Instance saved:"+str(self))

    def __str__(self):
        return  str([self.user])
