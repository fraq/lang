# read the file
# transform and re-save 
# make sure opened as UTF-8
import io
import time



def replace(line,search_string,replace_string):
        start = line.find(search_string)
        if start != -1:            
            newline = line.replace(search_string,replace_string)

#             end = start + len(search_string)
#             endp = min(end+3,len(line))
#             startp = max(0, start-3)             
#             print(line[startp:endp] + ' -> ' + newline)

            return newline 
        else:
            return line
    
def get_data(orig_filename,new_filename):
    with io.open(new_filename,'wt',encoding='utf8') as f_out:
#        f_out.write("BEGIN;")
    
        with io.open(orig_filename,'rt',encoding='utf8') as f_in:
            for line in f_in:
                print (line)
                if line.startswith('SET '):
                    continue # don't output
                if line.find('TRIGGER ALL;') != -1:
                    continue # don't output
                if line.startswith('SELECT pg_catalog.'):
                    continue
                line = replace(line,"true","1")
                line = replace(line,"false","0")
                f_out.write(line)
#        f_out.write("END;")

def get_schema(orig_filename,new_filename):
    with io.open(new_filename,'wt',encoding='utf8') as f_out:
        with io.open(orig_filename,'rt',encoding='utf8') as f_in:
            creating = False
            for line in f_in:
                if line.startswith('CREATE TABLE'):
                    creating = True
                if creating:
                    line = replace(line,' id integer NOT NULL','id integer NOT NULL PRIMARY KEY AUTOINCREMENT')
                    f_out.write(line)
                if line.startswith(');'):
                    creating = False
        
get_data("localdump2.sql","data_dump.sql")
get_schema("schema2.BACKUP","schema.sql")
