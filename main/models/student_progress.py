from django.db import models
from django.contrib.auth.models import User
from datetime import date
from main.models import Profile
from datetime import date, datetime, timedelta

# Intermediate table between user and course. Also tracks stage, but stage may be null.
# StudentProgress records a student's progress through a language learning course.

class StudentProgress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True)
    course = models.ForeignKey('Course', on_delete=models.CASCADE, db_index=True)
    native_language = models.ForeignKey('Language', on_delete=models.CASCADE) # ties the course's target language to a specific native language
    stage = models.ForeignKey('Stage',blank=True,null=True, on_delete=models.SET_NULL)
    phase = models.CharField(default='', max_length=20)
    mode = models.CharField(default='', max_length=40)

    def last_checkin_date(self):
        last_checkin_date = Profile.objects.getval(sp=self,entry='last_checkin')
        if last_checkin_date:
            return datetime.strptime(last_checkin_date,'%Y-%m-%d').date()
        else:
            return date.today() - timedelta(7)
       
    def days_since_last_checkin(self):
        return (date.today() -self.last_checkin_date()).days

    def new_check_in_valid(self):
        return self.days_since_last_checkin() > 3


    def save(self, *args, **kwargs):
        super(StudentProgress, self).save(*args, **kwargs)

    def __str__(self):
        return str([self.user, self.course, self.stage])

    class Meta:
        verbose_name_plural = "Student Progressions"
        unique_together = ("user", "course")
