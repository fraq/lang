import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.forms import formset_factory
from django.http.response import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from grammar.models import *  # GramConcept, , Grammar, , StudentProgress, Course, Question, GrammarProgress
from main.models import Stage, Language, Sentence, Profile
from main.views.main import get_sp_or_redirect

from ..forms import *  # CreateQuestionForm, CreateGrammarForm


@permission_required('grammar.content_creator')
def integrity_check(request):
    log = logging.getLogger(__name__)
    English=Language.objects.get(id=1)
    
    log.debug('--Checking Grammars --')
    for grammar in Grammar.objects.all():
        gram_description = "Grammar ID:{} ({})".format(grammar.id,grammar.title_in(English))
        if grammar.title_in(English) is None:
            log.info("{}...No English title".format(gram_description))
            gram_title = grammar.title
            if grammar.title is None:
                log.info("{}...No title in any language".format(gram_description))
        if grammar.stage is None:
            log.info("{}...Not staged".format(gram_description))
        if grammar.gram_concept is None:
            log.info("{}...No GramConcept".format(gram_description))
        if not grammar.example_questions.exists():
            log.info("{}...No Example Questions".format(gram_description))
        if not grammar.score:
            log.info("{}...No score".format(gram_description))

    log.debug('--Checking Questions--')
    used_front_sentences={}
    used_back_sentences={}
    for question in Question.objects.all():
        # Checking Question sentences match languages
        target_lang_found = False
        native_lang_found = False
        if question.front.language == question.target_language:
            target_lang_found = True
            if question.back.language == question.native_language:
                native_lang_found = True
        elif question.front.language == question.native_language:
            native_lang_found = True
            if question.back.language == question.target_language:
                target_lang_found = True
        if not (target_lang_found and native_lang_found):
            log.info("Question with mismatch languages- ID:{} ({}-{}) ({}-{})".format(question.id,question.front.language,question.back.language,question.native_language,question.target_language))

        # Checking Question grammars match targetsentence grammars        
        if not question.targetsentence().grammars.exists():
            log.info("Question target sentence - ID:{} has no grammars".format(question.targetsentence().id))
        elif question.grammar not in question.targetsentence().grammars.all():
            log.info("grammar for Question ID:{} not found in targetsentence ID:{}".format(question.id,question.targetsentence().id))

        # checking Questions have scores
        if question.score is None:
            log.info("Question score not found- ID:{}".format(question.id))
    
        # checking for duplicate Questions
        if question.front.id not in used_front_sentences:
            used_front_sentences[question.front.id]=question.id
        else:
            log.info("Question duplicating front sentence- ID:{} and ID:{} for sentence:{}".format(question.id,used_front_sentences[question.front.id],question.front.text))
        if question.back.id not in used_back_sentences:
            used_back_sentences[question.back.id]=question.id
        else:
            log.info("Question duplicating back sentence- ID:{} and ID:{} for sentence:{}".format(question.id,used_back_sentences[question.back.id],question.back.text))        

        # Checking for identical duplicate entries for the same question
        last_simplified='xxx'
        last_entry_id = 0
        for entry in question.entry_set.all().order_by('simplified'):
            if entry.simplified == last_simplified:
                log.info("Duplicate entry for same question - ID:{} and {} simplified:{} question:{}".format(entry.id,last_entry_id,entry.simplified, question))
            last_entry_id = entry.id
            last_simplified = entry.simplified

@permission_required(['grammar.add_grammar','grammar.change_grammar'],raise_exception=True)
def gram_update(request, pk=None):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    course = sp.course
    native_language = sp.native_language
    
    initial = {'native_language':native_language.id,    
                'target_language':course.target_language.id,
                'Title': None,             
                'Explanation': None,
                 'gram_concept': None,   
                 'stage':None,
                 'id':None,  
               }
    context = {}
    context['title'] = 'Create New Grammar'
    if pk is not None: # empty update, or saving an update
        context['title'] = 'Update Grammar'
        grammar = get_object_or_404(Grammar, id=pk)
        initial.update({'Title': grammar.title_in(native_language),             
                'Explanation': grammar.explanation_in(native_language),
                'gram_concept':str(grammar.gram_concept.id),   
                'stage':grammar.stage.id,
                'id':grammar.id,  
                       })   

    # populate form options
    stage_choices = []
    gram_concept_choices = []
    for stg in course.stage_set.all():
        stage_choices.append((stg.id,stg.title_in(native_language)))
    for gc in GramConcept.objects.all():
        gram_concept_choices.append((gc.id,gc.title_in(native_language)))

    if request.method == 'POST': # saving
        form = UpdateGrammarForm (request.POST, initial=initial, stage_choices=stage_choices, gram_concept_choices=gram_concept_choices)
        if form.is_valid():
            (result,grammar_id) = form.save(request)
            if result in ('Created','Updated'):
                messages.add_message(request, messages.SUCCESS, 'Grammar successfully saved')
                if result == 'Created':
                    return redirect ('grammar:new_question_for_grammar',grammar_id=grammar_id )                        
            return redirect ('grammar:create_dashboard')
    else:
        form = UpdateGrammarForm (None, initial=initial, stage_choices=stage_choices, gram_concept_choices=gram_concept_choices)
    context['form']= form
    return render(request, 'grammar/gram_update.html', context)
    
@permission_required('grammar.content_creator')
def create_dashboard(request):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    course = sp.course

    # Populates existing grammars assigned to stages of current course
    stages_raw = course.stage_set.all()
    countlist = {}
    stage_titles = {}
    stage_ids = {}
    grammar_titles = {}
    grammar_concepts = {}
    grammar_explanation = {}
    grammar_example_question = {}
    grammar_id = {}
    gram_stage = {}
    questions = {}
    stages = {}
    counter = 0
    for stg in stages_raw:
        countlist[counter] = "stage"
        stage_titles[counter] = stg.title_in(sp.native_language)
        stage_ids[counter] = stg.id
        stages[counter] = {'title':stg.title_in(sp.native_language),
                           'id':stg.id
                           }
        last_stage_title = stg.title_in(sp.native_language)
        counter += 1

        relevant_grammars = stg.grammar_set.all().order_by('score')
        for grm in relevant_grammars:
            # mark those without example sentence, explanation, score. dropdown to assign grammar to new stage</p>
            countlist[counter] = "grammar"
            grammar_titles[counter] = grm.title_in(sp.native_language)
            grammar_concepts[counter] = grm.gram_concept.title
            grammar_id[counter] = grm.id
            questions[counter] = Question.objects.filter(grammar=grm.id).count()
            stage_titles[counter] = last_stage_title
            grammar_explanation[counter]= (grm.explanation_in(sp.native_language) != "None")
            grammar_example_question[counter]= grm.example_questions.filter(grammar=grm,
                                                                            target_language=course.target_language,
                                                                            native_language=sp.native_language
                                                                            ).exists()    
            counter += 1

    context = {
        'course': course, 
        'stages': stages,
        'countlist': countlist,
        'stage_titles': stage_titles,
        'stage_ids': stage_ids,
        'grammar_titles': grammar_titles,
        'grammar_concepts': grammar_concepts,
        'grammar_explanation': grammar_explanation,
        'grammar_example_question': grammar_example_question,
        'grammar_id': grammar_id,
        'questions': questions, # the number of questions who identify question.grammar as this grammar
        'uninstantiated_gram_concepts': GramConcept.objects.uninstantiated(sp.course.target_language), # used in gram_create_snippet.html
        'inst_gram_concepts': GramConcept.objects.instantiated(sp.course.target_language), # used in gram_create_snippet.html
    }
    return render(request, 'grammar/create_dashboard.html', context)

@permission_required('grammar.change_grammar',raise_exception=True)
def grammar_restage(request, pk):
    stage = get_object_or_404(Stage, id = request.POST['stage'])
    grammar = get_object_or_404(Grammar, id = pk)
    course = stage.course

    # confirm grammar and stage are appropriately matched
    if course.target_language != grammar.target_language:
        raise Http404("This grammar is for a different language to the Stage's course")

    # reset grammar to new stage
    grammar.stage = stage
    grammar.save()
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + " restaged grammar "+ str(grammar)+" to stage "+str(stage))
    return redirect('grammar:create_dashboard')
    
    
@permission_required(['grammar.add_question','grammar.change_question'],raise_exception=True)
def update_question(request, question_id=None, grammar_id=None, sentence_id=None):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    if 'selected_stages' in request.session:
        selected_stages = request.session['selected_stages'] # keeps tab open from previous create
        selected_grammars = request.session['selected_grammars']
    else:
        selected_stages = None
        selected_grammars = None

    course = sp.course
    all_stages = []
    context = {}
    for stg in course.stage_set.all():
        grammars = []
        for grammar in stg.grammar_set.all():
            grammars.append({'title':grammar.full_title(sp.native_language),'id':grammar.id})
        all_stages.append({'title':stg.title_in(sp.native_language),'id':stg.id,'grammars':grammars})
    # Initial is used in populating form fields
    initial = {
            # fields that appear as Form Fields
            'NativeSentence': '',
            'TargetSentence': '',
            'Hint': '',
            'Explanation': '',
            'NativeFront': 'on', # Native on front
            'native_language':sp.native_language.id,
            'target_language':course.target_language.id,   
            'question_id': None if question_id == None else int(question_id),
            # not in the form fields, but popped by __init__
            'grammar_choices': Grammar.objects.filter(stage__course=course),
            'stage_choices': course.stage_set.all(),
            'selected_stages': selected_stages, 
            'selected_grammars': selected_grammars,
        }
    # context is info not in the form fields but used to render the template
    context = {
            'selected_stages': selected_stages, 
            'selected_grammars': selected_grammars,
            'all_stages': all_stages,
            'course': course, 
            'native_language': sp.native_language,
            'target_language':course.target_language,     
            'title': 'Create New Question',  
            'question_id': None if question_id == None else int(question_id),
        }
    if question_id is not None: # update mode
        context['title'] = 'Update Question'
        question = get_object_or_404(Question, id=question_id)
        # find all grammars, work out which stages they belong to, open those tabs
        selected_stages = set()
        selected_grammars = set()
        for grm in question.targetsentence().grammars.all():
            selected_stages.add(grm.stage.id)
            selected_grammars.add(grm.id)
        initial.update({
                'NativeSentence': question.nativesentence().text,
                'TargetSentence': question.targetsentence().text,
                'Hint': question.hint,
                'Explanation': question.explanation,
                'NativeFront': ('on' if question.native_front() else 'x'), # 'on'=native goes on front 
                'selected_grammars': list(selected_grammars),
            })
        context.update({
                'selected_stages': list(selected_stages),
                'selected_grammars': list(selected_grammars),
            })
    elif grammar_id is not None:
        grm = get_object_or_404(Grammar, id = int(grammar_id))
        selected_stages = [grm.stage.id]
        selected_grammars = [grm.id]
        initial.update({
                'selected_grammars': list(selected_grammars),
            })
        context.update({
                'selected_stages': list(selected_stages),
                'selected_grammars': list(selected_grammars),
                'title': 'Question for Grammar '+grm.title_in(sp.native_language),
            })
    elif sentence_id is not None:
        target_sentence = get_object_or_404(Sentence, id = int(sentence_id))  
        selected_stages = set()
        selected_grammars = set()
        for grm in target_sentence.grammars.all():
            selected_stages.add(grm.stage.id)
            selected_grammars.add(grm.id)
        initial.update({
                'TargetSentence': target_sentence.text,
                'TargetSentence_id': target_sentence.id,
                'selected_grammars': list(selected_grammars),
            })
        context.update({
                'selected_stages': list(selected_stages),
                'TargetSentence_id': target_sentence.id,
                'selected_grammars': list(selected_grammars),
            })
    if request.method == 'POST':            
        NativeFront = 'on' if 'NativeFront' in request.POST else ''
        form = QuestionForm (request.POST, initial=initial)    
        if form.is_valid():
            form.save(request)
            messages.add_message(request, messages.SUCCESS, 'Question successfully saved')        
            if question_id is not None: # update mode
                request.session['selected_stages'] = None
                request.session['selected_grammars'] = None
                return redirect('grammar:create_dashboard')
            else: # create mode
                # remember selected stages
                selected_stages = set()
                selected_grammars = set()
                try:
                    for grm in request.POST.getlist("GrammarsIncluded[]"):
                        selected_stages.add(grm.stage.id)
                        selected_grammars.add(grm.id)
                    request.session['selected_stages'] = list(selected_stages)
                    request.session['selected_grammars'] = list(selected_grammars)
                except:
                    pass                
                return redirect('grammar:new_question')
    else:
        form = QuestionForm (None, initial=initial)
    context['form'] = form
    return render(request, 'grammar/new_question.html', context)

@permission_required('grammar.add_question',raise_exception=True)
def new_question_from_sentence(request):
    pass

@permission_required('grammar.content_creator',raise_exception=True)
def example_questions(request,grammar_id):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    
    grammar_id = int (grammar_id)
    course = sp.course
    grammar = get_object_or_404(Grammar, id = grammar_id)    
    native_languages = Student.objects.get(user=request.user).list_native_languages()
    # find all sentences that contain this grammar
    example_sentence_ids = grammar.sentence_set.order_by().values_list('id',flat=True)
    # find all questions that are in the course's native language 
    example_sentences = grammar.sentence_set.all()
    front_related_questions = Question.objects.filter(front__in=example_sentence_ids).filter(back__language__in=native_languages)
    front_related_sentences = front_related_questions.values_list('front',flat=True)
    back_related_questions = Question.objects.filter(back__in=example_sentence_ids).filter(front__language__in=native_languages)  
    back_related_sentences = back_related_questions.values_list('back',flat=True)
    sentences_outside_questions = example_sentences.exclude(id__in=back_related_sentences).exclude(id__in=front_related_sentences)

    output={}
    counter=0
    # build list
    if back_related_questions.exists():
        output[counter] = {'title': str(sp.native_language) + '->' + str(course.target_language) + ' questions'}
        counter += 1
        for question in back_related_questions:
                output[counter] = {'front':question.front.text,'back':question.back.text,'question_id':question.id,'belong':question.grammar.id == grammar_id, 'grammar_title':question.grammar.title_in(sp.native_language)}
                counter += 1
    if front_related_questions.exists():
        output[counter] = {'title': str(course.target_language) + '->' + str(sp.native_language) + ' questions'} 
        counter += 1
        for question in front_related_questions:
                output[counter] = {'front':question.front.text,'back':question.back.text,'question_id':question.id,'belong':question.grammar.id == grammar_id, 'grammar_title':question.grammar.title_in(sp.native_language)}
                counter += 1
    if sentences_outside_questions.exists():
        output[counter] = {'title': "Sentences currently not included in Questions"} 
        counter += 1
        for sentence in sentences_outside_questions:
                output[counter] = {'front':sentence.text,'back':"<Click to Create a matching translation>" ,'sentence_id':sentence.id}
                counter += 1

    context = {
        'output': output,
        'grammar': grammar,
        'grammar_title': grammar.full_title(sp.native_language),
        'native_language':sp.native_language,
        'target_language':course.target_language,
    }
    return render(request, 'grammar/example_questions.html', context)

@permission_required('main.ratify',raise_exception=True)        
def ratify_questions(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": ratify_questions started")
    
    native_langs = Student.objects.get(user=request.user).list_native_languages()
    questions_remaining = 5 # how many questions to show user per page
    RatifyQuestionFormSet = formset_factory(RatifyQuestionForm, extra=0)    
    if request.POST:
        formset = RatifyQuestionFormSet(request.POST)
        for form in formset:
            if form.is_valid():         
                form.save(request)   
    # Populate forms: Look for un-approved questions. Look for questions with sentences native to the user that have insufficient approvals. 
    # First, question target sentences:
    target_selection =[]
    for question in Question.objects.filter(approval=None).filter(target_language__in=native_langs):
        # check if side native to user is incomplete...
        if (question.targetsentence().approval is None and 
                questions_remaining and 
                not question.targetsentence().ratifications.filter(author=request.user).exists()
                ):            
            target_selection.append({'sentence':question.targetsentence().id,
                                     'assessment':'',
                                     'alternative':None,
                                     'question':question.id,
                                     'hint':question.hint,
                                     'prompt':question.nativesentence().text,
                                     'candidate_text':question.targetsentence().text,
                                     })
            questions_remaining -= 1

    native_selection =[]
    for question in Question.objects.filter(approval=None).filter(native_language__in=native_langs):
        if (question.nativesentence().approval is None and 
                questions_remaining and
                not question.nativesentence().ratifications.filter(author=request.user).exists()
                ):
            native_selection.append({'sentence':question.nativesentence().id,
                                     'assessment':'',
                                     'alternative':None,
                                     'question':question.id,
                                     'hint':question.hint,
                                     'prompt':question.targetsentence().text,
                                     'candidate_text':question.nativesentence().text,
                                     })
            questions_remaining -= 1
    
    context = {
        'formset': RatifyQuestionFormSet(initial=target_selection + native_selection),
    }
    return render(request, 'grammar/ratify_questions.html', context)

@permission_required('main.ratify',raise_exception=True)        
def view_ratifications(request):
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": view_ratifications started")
    under_ratification_sentences = {}
    # cycle through all un-approved sentences under ratification
    for sentence in Sentence.objects.exclude(approval=True).exclude(ratifications=None):        
        under_ratification_sentences[sentence.id] = sentence.ratifications.all()
    # locate corresponding questions
    under_ratification_sentence_ids = list(under_ratification_sentences.keys())
    front_q_ids = Question.objects.exclude(approval=True).filter(front__in=under_ratification_sentence_ids).values_list('id',flat=True)
    back_q_ids = Question.objects.exclude(approval=True).filter(back__in=under_ratification_sentence_ids).values_list('id',flat=True)    
    under_ratification_question_ids = list(set(front_q_ids) | set(back_q_ids)) # removes duplicate ids
    # prepare context for view
    questions = {'Reject':[],'Mixed':[],'Approve':[]}
    for q_id in under_ratification_question_ids:
        question = Question.objects.get(id=q_id)
        type = None
        grammar_list = ""
        if question.front.ratifications.filter(assessment=Ratification.REJECT) or question.back.ratifications.filter(assessment=Ratification.REJECT):
            type = 'Reject'
        if question.front.ratifications.filter(assessment=Ratification.APPROVE) or question.back.ratifications.filter(assessment=Ratification.APPROVE):
            if type:
                type = 'Mixed'
            else:
                type = 'Approve'
        if type is None:
            # the user passed on ratifying
            continue
        else:
            if question.front.ratifications.exists():
                for grammar in question.front.grammars.all():
                    if grammar_list != "":
                        grammar_list += ", "
                    grammar_list += grammar.title_in(sp.native_language)  
            if question.back.ratifications.exists():
                for grammar in question.back.grammars.all():
                    if grammar_list != "":
                        grammar_list += ", "
                    grammar_list += grammar.title_in(sp.native_language)  
        questions[type].append({'id':q_id,
                          'front':question.front.text,
                          'back':question.back.text,
                          'question':question,
                          'front_ratifications':under_ratification_sentences[question.front.id] if question.front.id in under_ratification_sentences else None,
                          'back_ratifications':under_ratification_sentences[question.back.id] if question.back.id in under_ratification_sentences else None,
                          'grammars':grammar_list,            
            })
    context = {
        'questions': questions,
    }
    return render(request, 'grammar/view_ratifications.html', context)

def apply_ratification_update(request, pk):
    # I have a ratification number
    rat = Ratification.objects.get(pk=pk)
    # from it get the sentence
    sentence = rat.sentence_set.all().first()
    # update text
    sentence.text = rat.alternative
    sentence.ratifications.all().delete() # deletes all ratifications associated with this sentence    
    sentence.save()
    return redirect ('grammar:view_ratifications')

        
class GramConceptCreate(PermissionRequiredMixin,CreateView):
    permission_required = 'grammar.add_gramconcept'
    model = GramConcept
    fields = '__all__'
    template_name = 'main/model_create.html'
    success_url = reverse_lazy('grammar:gramconcept_list')

    def get_context_data(self, **kwargs):
        # This method allows us to add extra context information to the generic view
        context = super().get_context_data(**kwargs)
        context.update({
                'list_url':'grammar:gramconcept_list',
                'model_name':'Grammar Concept'
            })
        return context    

        
class GramConceptUpdate(PermissionRequiredMixin,UpdateView):
    permission_required = 'grammar.change_gramconcept'
    model = GramConcept
    fields = '__all__'
    template_name = 'main/model_update.html'
    success_url = reverse_lazy('grammar:gramconcept_list')

    def get_context_data(self, **kwargs):
        # This method allows us to add extra context information to the generic view
        context = super().get_context_data(**kwargs)
        context.update({
                'create_url':'grammar:gramconcept_create',
                'list_url':'grammar:gramconcept_list',
                'model_name':'Grammar Concept'
            })
        return context    

class GramConceptDelete(PermissionRequiredMixin,DeleteView):
    permission_required = 'grammar.delete_gramconcept'
    model = GramConcept
    template_name = 'main/model_confirm_delete.html'
    success_url = reverse_lazy('grammar:gramconcept_list')

    def get_context_data(self, **kwargs):
        # This method allows us to add extra context information to the generic view
        context = super().get_context_data(**kwargs)
        context.update({
                'list_url':'grammar:gramconcept_list',
                'model_name':'Grammar Concept'
            })
        return context    

class GramConceptList(PermissionRequiredMixin,ListView):    
    permission_required = 'grammar.content_creator'
    model = GramConcept
    template_name = 'main/model_list.html'

    def get_context_data(self, **kwargs):
        # This method allows us to add extra context information to the generic view
        context = super().get_context_data(**kwargs)
        context.update({
                'update_url':'grammar:gramconcept_update',
                'delete_url':'grammar:gramconcept_delete',
                'create_url':'grammar:gramconcept_create',
                'model_name':'Grammar Concept'
            })
        return context    
    

class GrammarDelete(PermissionRequiredMixin,DeleteView):
    permission_required = 'grammar.delete_grammar'
    model = Grammar
    template_name = 'main/model_confirm_delete.html'
    success_url = reverse_lazy('grammar:create_dashboard')

class QuestionDelete(PermissionRequiredMixin,DeleteView):
    permission_required = 'grammar.delete_question'
    model = Question
    template_name = 'main/model_confirm_delete.html'
    success_url = reverse_lazy('grammar:create_dashboard')
