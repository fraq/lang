from django import forms
from grammar.models import Question, Grammar, GramConcept
from main.models import Course, Language, Comment, Stage, Sentence, Ratification, Profile, Student
from django.forms import ModelMultipleChoiceField
from django.shortcuts import get_object_or_404
import logging
from tinymce.widgets import TinyMCE

class RatifyQuestionForm(forms.Form):
    prompt = forms.CharField(max_length=150,required=False, widget=forms.TextInput())
    candidate_text = forms.CharField(max_length=150,required=False, widget=forms.TextInput())
    hint = forms.CharField(max_length=150,required=False)
    assessment = forms.ChoiceField(required=True, choices=([('','---------')] + list(Ratification.R_CHOICES)))   
    alternative = forms.CharField(max_length=150,required=False, widget=forms.TextInput())
    sentence = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    question = forms.IntegerField(widget=forms.HiddenInput(),required=True)

#     def __init__(self, *args, **kwargs): 
#         super(RatifyQuestionForm, self).__init__(*args, **kwargs) 

    def save(self,request):
        log = logging.getLogger(__name__)
        data = self.cleaned_data
        # create approval record
        ratification = Ratification.objects.create(
                        assessment=data['assessment'],
                        alternative=data['alternative'],
                        author=request.user)
        sentence = Sentence.objects.get(id=data['sentence'])
        sentence.ratifications.add(ratification)
        # calculate if master sentence is now approved
        rejections = 0
        approvals = 0
        for ratf in sentence.ratifications.all():
            if ratf.assessment == Ratification.APPROVE:
                approvals += 1
            elif ratf.assessment == Ratification.REJECT:
                rejections += 1
        if rejections > 2: 
            sentence.approval = False
        elif approvals + 2 > rejections:
            sentence.approval = True
        else:
            pass # still under review
        sentence.save()
        # calcuate if question is now approved
        question = Question.objects.get(id=data['question'])
        if question.front.approval == False or question.back.approval == False:
            question.approval = False
            question.save()
        elif question.front.approval == True and question.back.approval == True:
            question.approval = True
            question.save()
        log.info(("User " + request.user.username + " ratified sentence " + str(sentence) + " from question " + str(question)).encode('utf_8'))

class UpdateGrammarForm(forms.Form):
    UNASSIGNED_GRAMCONCEPT = 1
    
    id = forms.IntegerField(widget=forms.HiddenInput(),required=False)
    target_language = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    native_language = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    Title = forms.CharField(max_length=100,required=True, widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))
    Explanation = forms.CharField(required=False, widget=TinyMCE(mce_attrs={'cols': 20, 'required': False}))
    stage = forms.TypedChoiceField(required=True, choices=(), coerce=int)    
    gram_concept = forms.TypedChoiceField(required=False, empty_value=UNASSIGNED_GRAMCONCEPT, choices=(), coerce=int)

    def __init__(self, *args, **kwargs): 
        stage_choices = kwargs.pop('stage_choices') 
        gram_concept_choices = kwargs.pop('gram_concept_choices') 
        super(UpdateGrammarForm, self).__init__(*args, **kwargs) 
        self.fields['stage'].choices = [('','---------')] + stage_choices
        if gram_concept_choices is None: # user not authorized to modify
            del self.fields['gram_concept']
        else:
            self.fields['gram_concept'].choices = [('','---------')] + gram_concept_choices

    def save(self,request):
        log = logging.getLogger(__name__)
        data = self.cleaned_data
        native_language = get_object_or_404(Language, id=data['native_language'])
        target_language = get_object_or_404(Language, id=data['target_language'])
        if data['id'] is None: # creating a new grammar

            # create sub-components first
            if len(data['Explanation']) == 0:
                new_expl = None
            else:
                # create explanation entry
                new_expl = Comment.objects.create(
                    text = data['Explanation'],
                    language = native_language,
                    author = request.user,
                    ) 
            new_title = Sentence.objects.create(
                text = data['Title'],
                language = native_language,
                author = request.user,            
                )

            # create grammar entry            
            new_gram = Grammar.objects.create(
                gram_concept = GramConcept.objects.get(id=data['gram_concept']),
                target_language = target_language,
                author=request.user,
                title=new_title,   
                explanation=new_expl,
                stage=Stage.objects.get(id=data['stage']),         
            )

            log.info("User " + request.user.username + " created grammar "+ str(new_gram))
            return ('Created',new_gram.id)
        else: # updating existing grammar
            grammar = get_object_or_404(Grammar, id=data['id'])
            if self.has_changed():
                if 'Title' in self.changed_data:
                    # see if exists in the user's language
                    if native_language in grammar.available_languages():
                        title = grammar.locate_title_in(native_language)
                        title.text = data['Title']
                        title.save()
                    else:
                        new_title = Sentence.objects.create(
                            text = data['Title'],
                            language = native_language,
                            author = request.user,
                            parent = grammar.title,      
                            )
                if 'Explanation' in self.changed_data:
                    # find explanation in the native_language, or create a new entry in the user's language
                    explanation = Comment.objects.locate_in(grammar.explanation,native_language)
                    if explanation is None:
                        new_expl = Comment.objects.create(
                            text = data['Explanation'],
                            language = native_language,
                            author = request.user,
                            parent = None,
                            )      
                        grammar.explanation = new_expl    
                    elif explanation.language != native_language:                        
                        new_expl = Comment.objects.create(
                            text = data['Explanation'],
                            language = native_language,
                            author = request.user,
                            parent = grammar.explanation,
                            )      
                    else:
                        explanation.text=data['Explanation']
                        explanation.author = request.user
                        explanation.save()
                if 'stage' in self.changed_data:
                    grammar.stage = Stage.objects.get(id=data['stage'])
                if 'gram_concept' in self.changed_data:
                     grammar.gram_concept = GramConcept.objects.get(id=data['gram_concept'])                 
                grammar.author = request.user
                grammar.save()
                log.info("User " + request.user.username + " updated Grammar " + str(grammar))
                return ('Updated',grammar.id)
            else:
                return (False,data['id']) # no change, no save

class QuestionForm(forms.Form):
    NativeSentence = forms.CharField(max_length=200, label='Native language sentence', widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))
    TargetSentence = forms.CharField(max_length=200, label='Target language sentence')
    TargetSentence_id  = forms.IntegerField(widget=forms.HiddenInput(),required=False) # a sentence already exists
    Hint = forms.CharField(required=False, widget=TinyMCE(mce_attrs={'cols': 20, 'required': False}))
    Explanation = forms.CharField(required=False, widget=TinyMCE(mce_attrs={'cols': 20, 'required': False}), help_text="Written in the native language, an explanation to peculiarities in the answer")
    NativeFront = forms.CharField(required=False)
    target_language = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    native_language = forms.IntegerField(widget=forms.HiddenInput(),required=True)
    question_id = forms.IntegerField(widget=forms.HiddenInput(),required=False)

    def __init__(self, *args, **kwargs): 
        # Requires: self.initial['grammar_choices', 'selected_grammars', 'stage_choices', 'selected_stages']
        super(QuestionForm, self).__init__(*args, **kwargs)         
        self.fields['GrammarsIncluded[]'] = ModelMultipleChoiceField(queryset=self.initial['grammar_choices'],
                                                                     label = 'Featured Grammars',
                                                                     initial = self.initial['selected_grammars'],      
                                                                     required=True,                                                               
                                                                     )
        self.fields['selected_stages[]'] = ModelMultipleChoiceField(
                                queryset=self.initial['stage_choices'],
                                initial = self.initial['selected_stages'], 
                                required=False)


    def save(self,request):
        # Requires: self.cleaned_data['native_language', 'target_language', 'GrammarsIncluded[]']
        log = logging.getLogger(__name__)
        data = self.cleaned_data
        native_language = get_object_or_404(Language, id=data['native_language'])
        target_language = get_object_or_404(Language, id=data['target_language'])

        GrammarsIncluded = data['GrammarsIncluded[]']        
        if data['question_id'] is None: # creating a new question
            # create sentence objects
            native_sentence = Sentence.objects.create(
                text = data['NativeSentence'],
                language = native_language,
                author = request.user, 
                parent = None)
            # if this is user's language, immediately create an approval record
            if native_language.id in Student.objects.get(user=request.user).list_native_languages():
                ratification = Ratification.objects.create(
                        assessment=Ratification.APPROVE,
                        author=request.user)
                native_sentence.ratifications.add(ratification)
                native_sentence.save()

            if data['TargetSentence_id'] is None: # creating a new sentence
                target_sentence = Sentence.objects.create(
                    text = data['TargetSentence'],
                    language = target_language,
                    author = request.user, 
                    parent = native_sentence)
                # if this is user's language, immediately create an approval record
                if target_language.id in Student.objects.get(user=request.user).list_native_languages():
                    ratification = Ratification.objects.create(
                            assessment=Ratification.APPROVE,
                            author=request.user)
                    target_sentence.ratifications.add(ratification)
                    target_sentence.save()

            else:
                target_sentence = Sentence.objects.get(id=int(data['TargetSentence_id']))                
            target_sentence.grammars.add(*GrammarsIncluded)
            if data['NativeFront'] == 'on':
                front, back = native_sentence, target_sentence
            else:
                front, back = target_sentence, native_sentence            
            # create question object
            question = Question.objects.create(
                front = front,
                back = back,
                hint = data['Hint'],
                explanation = data['Explanation'],
                author = request.user,
                target_language = target_language,
                native_language = native_language,
            ) 
            log.info("User " + request.user.username + " created question "+ str(question))
        else: # updating existing question
            question = get_object_or_404(Question, id=data['question_id'])
            question.hint = data['Hint']
            question.explanation = data['Explanation']
            native_sentence = question.nativesentence()
            target_sentence = question.targetsentence()
            if data['NativeFront'] == 'on':
                question.front, question.back = native_sentence, target_sentence
            else:
                question.front, question.back = target_sentence, native_sentence            
                             
            if 'NativeSentence' in self.changed_data:
                native_sentence.text=data['NativeSentence']
                native_sentence.author = request.user
                native_sentence.save()
            if ('TargetSentence' in self.changed_data) or ('GrammarsIncluded[]' in self.changed_data):
                target_sentence.text=data['TargetSentence']
                target_sentence.author = request.user
                target_sentence.save()
                target_sentence.grammars.clear()
                target_sentence.grammars.add(*GrammarsIncluded) # set replaces previous associations with new list
            question.author = request.user
            question.save()
            log.info(("User " + request.user.username + " updated question " + str(question)).encode('utf_8'))