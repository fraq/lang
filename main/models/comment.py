from django.db import models
from django.contrib.auth.models import User
import logging
from tinymce import HTMLField

class CommentManager(models.Manager):
    def locate_in(self,comment,languages):
        # languages can be a list of languages in preferred order or a singular element
        if comment is None:
            return None
        else:
            try:
                translated_child = None
                for lang in languages: # try all languages provided in order to search for a suitable option
                    if comment.lang == language:
                        return comment
                    else:
                        translated_child=Comment.objects.filter(parent=comment, language=lang).first()
                        if translated_child is None:
                            continue
                        else:
                            break
                return translated_child
            except:
                # languages is not a list, but a singular language
                lang = languages
                if comment.language == lang:
                    return comment
                else:
                    translated_child=Comment.objects.filter(parent=comment, language=lang).first()
                    if translated_child is not None:
                        return translated_child
                    # otherwise return root entry
                    else:
                        return comment

class Comment(models.Model):
    """
    A larger text block useful for comments to be attached by any model instance.  
    
    Because it can be used for a variety of different models, it does not carry a foreignkey itself, but will be 
    pointed to by the model object being commented on. 
    """

    text = HTMLField('Comment')
    language = models.ForeignKey('Language', null=True, on_delete=models.CASCADE)
    parent = models.ForeignKey('Comment', null=True, blank=True, on_delete=models.CASCADE, db_index=True, help_text="If this comment is in reply to a previous comment")
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    last_edited = models.DateField(auto_now=True)
    objects = CommentManager()

    def __str__(self):
        return str(self.text)

    def save(self, *args, **kwargs):
        super(Comment, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-last_edited']