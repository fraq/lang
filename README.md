# GrowFastGrowDeep

Please visit https://bitbucket.org/talkingtoaj/lang/wiki/ for complete instructions on how to contribute to this project.

### How do I get set up?

Full instructions are available at https://bitbucket.org/talkingtoaj/lang/wiki/

### tl;dr

1. Install [docker](https://www.docker.com/community-edition) and [docker-compose](https://docs.docker.com/compose/install/)
1. `docker-compose up`
1. Open a browser to http://127.0.0.1:8000 and confirm you can log in with username and password of `admin` and `qweqweqwe`

### Does the app know my code changed?

For most changes, the application in the docker container will pick them up right away. Refresh your browser or try to trigger the new feature you created. Some changes, like modifying `settings.py` will require that you restart the application. 

### Basic docker-compose usage

- If you started the container with `docker-compose up`, it runs in the foreground and takes over your terminal. You can stop it with `ctrl+c`. 
- If you want to run the container in the background so you can do other things in the same terminal session, use `docker-compose up -d`
- To restart the container
  - If the container is in the foreground, stop it with `ctrl+c` and run `docker-compose up` again
  - If the container is in the background, `docker-compose restart` will restart the stack.
  - If you want a fresh container, `docker-compose down -v && docker-compose up`

  
 ### Installation ###
 On first run, you must run grammar/reset_all (you will need admin privileges) in order to install some initial fixtures needed for proper operation.