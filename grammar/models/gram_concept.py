from django.db import models
from django.contrib.auth.models import User
from .grammar import Grammar
from common.MixinClasses import TranslatableMixin
import logging

# A Grammar Concept gives the abstract linguistical name to grammars as implemented in
# different languages. Future tense is expressed in varying grammars in different languages
# but future tense as a gramattical concept will exist in most.
# Grammar concepts help tie sentences of different languages together as related.

class GrammarConceptManager(models.Manager):
    def instantiated(self, target_language):
        """Returns all `:model:grammar.GramConcept`s that have a representative `:model:grammar.grammar` in this course"""
        inst_gram_concepts_id = Grammar.objects.filter(target_language=target_language).values_list('gram_concept', flat=True)
        return GramConcept.objects.filter(id__in= inst_gram_concepts_id)

    def uninstantiated(self, target_language):
        """Returns all `:model:grammar.GramConcept`s that do not have a representative `:model:grammar.grammar` in this course"""
        inst_gram_concepts_id = Grammar.objects.filter(target_language=target_language).values_list('gram_concept', flat=True)
        return GramConcept.objects.exclude(id__in= inst_gram_concepts_id)

class GramConcept(TranslatableMixin, models.Model):
    title = models.ForeignKey('main.Sentence', on_delete=models.PROTECT, help_text="Linguistical name")
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    last_edited = models.DateField(auto_now=True)
    objects = GrammarConceptManager()

    def save(self, *args, **kwargs):
        super(GramConcept, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model GramConcept Instance saved:"+str(self))

    class Meta:
        ordering = ['-last_edited'] 
        