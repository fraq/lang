from django.db import models
from common import css
from grammar.models import Question, GrammarProgress, Grammar
from main.models import Stage
from django.core.validators import validate_comma_separated_integer_list
from random import randint
from django.contrib.auth.models import User

# Tracks a student's grammar progress. Also tracks stage, but stage may be null.
# This, combined with GrammarProgress model sum up a student's progress through a language

class FocusGrammarProgress(models.Model):
    VARIANCE_MAX = 1000
    VARIANCE_CUTOFF_INITIAL_PLACEMENT = 400
    VARIANCE_MIN_REQUIRED  = 100
    FOCUS_GOAL = 2 # weekly assessment to quit after two focus grammars are located

    sp = models.OneToOneField('main.Studentprogress', on_delete=models.CASCADE)
    score = models.FloatField(default = 0)
    variance = models.FloatField(default = VARIANCE_MAX)
    stage = models.ForeignKey('main.Stage',blank=True,null=True, on_delete=models.SET_NULL)
    scheduled_qs = models.TextField(blank=True,null=True,validators=[validate_comma_separated_integer_list],default="")
    focuses = models.TextField(blank=True,null=True,validators=[validate_comma_separated_integer_list],default="")

    def lower_variance(self):
        self.variance = self.variance * 0.8
        self.save()

    def refresh_variance(self):
        """Increase the variance for elapsed days. Max variance of 1000, achieved if away for 21 days."""
        self.variance = min (self.VARIANCE_MAX, max(self.variance + self.sp.days_since_last_checkin()*47,self.VARIANCE_CUTOFF_INITIAL_PLACEMENT)) 
        self.save()

    def rescore(self,score):
        self.score = max(score, 0)
        self.save()        

    def save(self, *args, **kwargs):
        if self.stage is None:
            stage = self.sp.course.lowest_stage()
        super(FocusGrammarProgress, self).save(*args, **kwargs)

    def scheduled_qs_count(self):
        return css.length(self.scheduled_qs)

    def select_nearest_ELO_q(self, asked_qs):
        # find the question closest to the user's current ELO for grammars related to the current course
        target_score = max(self.score + randint(int(-self.variance*1.5),int(self.variance*1.5)),0)
        nearest_grammar_ids = Grammar.objects.nearest_grammars(self.sp.course,self.score).values_list('id', flat=True)
        possible_qs = Question.objects.filter(grammar__in=nearest_grammar_ids)
        shortened_list = possible_qs.exclude(id__in=asked_qs)
#        selected_q = shortened_list.filter().extra(select={"diff": "abs(score-%s)" % target_score}).order_by("diff").first()
        # since qs clump and we dont just want an outlier, we will randomly choose a q from the set remaining
        if shortened_list.count() > 0:
            random_index = randint(0, shortened_list.count() - 1)        
            selected_q = shortened_list.all()[random_index]
        else:
            selected_q = None 
        return selected_q
        
    def set_stage(self):
        # set the user's stage to that matching their ELO score, and generate the GrammarProgressions
        # start at bottom, work up through stages
        self.stage = None
        self.save()
        target_stage = Stage.objects.determine_stage(self.sp.course,self.score)
        while self.stage != target_stage:
            self.promote()
        
    def promote(self):
        # if the user's score permits it, raises the user to the next stage and creates appropriate GrammarProgresses
        # Optional stage_pos will ensure GrammarProgress.past_results is correctly set 
        if self.stage is None:
            self.stage = self.sp.course.lowest_stage()
        else:
            next_stage = Stage.objects.filter(position__gt=self.stage.position,course=self.sp.course).order_by("position").first()
            if next_stage is None: 
                # the user has achieved the highest stage there is
                return None
            else:
                self.stage = next_stage 
        for grammar in self.stage.grammar_set.all():
            if not GrammarProgress.objects.filter(fp=self,grammar=grammar).exists():
                gp = GrammarProgress.objects.create_gp(fp=self,grammar=grammar,stage_position=Stage.objects.determine_stage(self.sp.course,self.score).position)                
        self.save()
        return self.stage

    def __str__(self):
        return str([self.sp, self.stage])

    class Meta:
        verbose_name_plural = "Grammar Focus Progressions"
