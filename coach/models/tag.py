from django.db import models
from django.contrib.auth.models import User
from main.models import Language, Sentence
from common.MixinClasses import TranslatableMixin
import logging


class TagManager(models.Manager):
    def tags_in(self,language):
        available_tags=[]
        for tag in Tag.objects.all():
            available_tags.append((tag.id,tag.title_in(language)))
        return available_tags
    
    def lang_tag(self,language_id):
        # returns name of tag, first creates one if it does not yet exist
        English = Language.objects.English()
        language = Language.objects.get(id=language_id)
        tag_title="Lang:{}".format(language.title_in_English())        
        lang_tag_q = Tag.objects.filter(title__text=tag_title)
        if lang_tag_q.exists():
            lang_tag = lang_tag_q.first()
        else:
            # if tag does not yet exist, create
            lang_tag_title = Sentence.objects.create(
                text=tag_title,
                language=English,
                author=None,
                )
            lang_tag = Tag.objects.create(
                title=lang_tag_title,
                author=None,
                ) 
        return lang_tag
    
class Tag(TranslatableMixin, models.Model):
    """
    """
    title = models.ForeignKey('main.Sentence', on_delete=models.CASCADE, related_name='TagTitles')
    parent = models.ForeignKey('Tag',null=True,blank=True, on_delete=models.SET_NULL)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    objects = TagManager()

    def save(self, *args, **kwargs):
        super(Tag, self).save(*args, **kwargs)
    
    class Meta:
        ordering = ['-created']
        permissions = (
            ("content_creator", "Can create,read,update,delete"),
        )
        