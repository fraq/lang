from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from grammar.models import * #GramConcept, Grammar, Student, , Question, GrammarProgress
from main.models import *
from main.forms import SignUpCourseForm
from django.contrib import messages
from django.contrib.auth import logout
from common import css, template_filters
import logging
from django.contrib.auth.decorators import permission_required
from datetime import date


"""
    Four stages to weekly cycle - called by switchboard view
"""

def printout_session(request):
    log = logging.getLogger(__name__)
    for key in list(request.session.keys()):
        if not key.startswith("_"): # don't print system session keys
            log.debug("request.session[{}]={}".format(key,request.session[key]))
    

@login_required
def mode_not_completed(request):
    request.session['mode_completed'] = False
    return redirect('main:switchboard')

@login_required
def mode_completed(request, ):
    request.session['mode_completed'] = True
    return redirect('main:switchboard')

def switchboard(request, mode=None, completed=False):
    """
    Central home function for interrupted browsing sessions. If session is still active will redirect to place last left off, 
    otherwise will redirect user to first phase 
    """
    from .report import profile_user,website_induction,check_in,register_new_user
    from .assess import can_do_statements,choose_learning_area,listening_comprehension,reading_comprehension,vocab_level
    from grammar.views.assess import initial_grammar_test,weekly_grammar_level  
    from grammar.views.coaching import initial_grammar_report, weekly_grammar_report
    from .homework import review_summary
    from coach.views.main import offer_coaching_tips, offer_exercises, follow_up_homework, holistic_report

    if not request.user.is_authenticated:
        return redirect('splash')
        
    log = logging.getLogger(__name__)
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    # after 3 days, the student is eligible for a new checkin
    # any incomplete checkin is deleted after 3 days
    if sp.new_check_in_valid() and 'last_visited' not in request.session:
            sp.mode = ''     
            sp.phase = '' 
            request.session['last_visited'] = True
            request.session['completed'] = True
            sp.save()    
    try:
        completed = request.session['mode_completed']
    except:
        completed = False
    request.session['mode_completed'] = False
    
    switch_lookup = {
        # we repeat mode and the identical function name because to use eval() is considered extremely dangerous security vulnerability

            # new user
            ('REPORT','register_new_user'):(register_new_user,'REPORT','website_induction'),
            ('REPORT','website_induction'):(website_induction,'ASSESS','choose_learning_area'),

            # returning user
            ('',''):(profile_user,'REPORT','profile_user'),    # profile_user is also accessed via splash_page for anonymous users. anon users on completion are redirected to register_new_user 
            ('REPORT',''):(profile_user,'REPORT','profile_user'),    # profile_user is also accessed via splash_page for anonymous users. anon users on completion are redirected to register_new_user 
            ('REPORT','profile_user'):(profile_user,'REPORT','check_in'),
            ('REPORT','check_in'):(check_in,'REPORT','follow_up_homework'),
            ('REPORT','follow_up_homework'):(follow_up_homework,'ASSESS','choose_learning_area'), 

            # choose_learning_area redirects to one of the following
            ('ASSESS',''):(choose_learning_area,'ASSESS','choose_learning_area'),
            ('ASSESS','choose_learning_area'):(choose_learning_area,'ASSESS','choose_learning_area'),
            ('ASSESS','can_do_statements'):(can_do_statements,'ASSESS','choose_learning_area'),                
            ('ASSESS','initial_grammar_test'):(initial_grammar_test,'ASSESS','initial_grammar_report'),
            ('ASSESS','weekly_grammar_level'):(weekly_grammar_level,'ASSESS','weekly_grammar_report'),
            ('ASSESS','listening_comprehension'):(listening_comprehension,'ASSESS','choose_learning_area'),
            ('ASSESS','reading_comprehension'):(reading_comprehension,'ASSESS','choose_learning_area'),
            ('ASSESS','vocab_level'):(vocab_level,'ASSESS','choose_learning_area'),

            ('ASSESS','initial_grammar_report'):(initial_grammar_report,'ASSESS','choose_learning_area'),
            ('ASSESS','weekly_grammar_report'):(weekly_grammar_report,'ASSESS','choose_learning_area'),

            # common ending
            ('COACHING',''):(holistic_report,'COACHING','holistic_report'),            
            ('COACHING','holistic_report'):(holistic_report,'COACHING','offer_coaching_tips'), # Provide tips on how to work on this week's focuses. store these to appear in next week's REPORT phase
            ('COACHING','offer_coaching_tips'):(offer_coaching_tips,'HOMEWORK','offer_exercises'), # possibly provide resource links or drills for them to work on
            ('HOMEWORK','offer_exercises'):(offer_exercises,'HOMEWORK','review_summary'),
            ('HOMEWORK','review_summary'):(review_summary,'HOMEWORK','review_summary'),
        }
    if mode is None:
        mode = sp.mode
    phase = sp.phase

    if mode == '':
        completed = True # move to first assignment
    try:
        (current_func, next_phase, next_mode) = switch_lookup[phase,mode]    
    except:
        raise ValueError("User" + request.user.username + " switchboard encountered unrecognized combination: "+phase+":"+mode)
    if not completed:
        # return control to last mode before browser interruption
        log.debug("User" + request.user.username + " Switchboard - returning control back to "+phase+":"+mode)
        printout_session(request)
        return current_func(request)         
    else:        
        # redirect to phase function
        log.debug("User" + request.user.username + " Switchboard - redirecting from "+phase+":"+mode+" to "+next_phase+":"+next_mode)        
        sp.phase = next_phase
        sp.mode = next_mode
        sp.save()
        current_func = switch_lookup[sp.phase,sp.mode][0]          
        printout_session(request)
        return current_func(request)

@login_required
def get_sp_or_redirect(request):
    """Returns sp or False if  redirect('main:select_course') needs to be called
    """
    try:
        return StudentProgress.objects.get(id=request.session['sp_pk']),request
    except:
        sps = StudentProgress.objects.filter(user=request.user)
        if sps.count() == 1:
            sp = sps.first()
            request.session['sp_pk'] = sp.id
            return sp,request
        else:
            return None,request

@login_required
def select_course(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": select_course started")

    sps = StudentProgress.objects.filter(user=request.user)
    if sps.count() == 0:
        log.error("User" + request.user.username + ": select_course view: no sps found, this should not be possible. logging user out and giving error message")
        messages.add_message(request, messages.WARNING, 'It seems your account was incompletely registered due to a bug. Please contact the website administrator')
        logout(request)
        return logout_view(request)
    
    else:
        course_choices = [] # used by form object
        course_details = [] # used in template
        form_title = "Choose which course you would like to study"
        for sp in sps:
            native_language = sp.native_language
            course = sp.course
            course_title = course.long_title(native_language)
            course_code = sp.id
            course_health = Course.objects.get_health(course,native_language)
            course_choices.append((course_code,course_title))
            course_details.append({
                    'course_code':course_code,
                    'course_title':course_title,
                    'course_health':course_health,
                    'course':course,
                })            

    if request.method == "POST":
        form = SignUpCourseForm (request.POST, course_choices=course_choices)
        if form.is_valid():
            sp = StudentProgress.objects.get(id=request.POST['selection_code'])
            log = logging.getLogger(__name__)
            log.info("User" + request.user.username + " selected course "+ str(sp.course))
            for key in list(request.session.keys()):
                if not key.startswith("_"): # don't delete system session keys
                    del request.session[key]
            request.session['sp_pk'] = sp.id # repopulate with new relevant sp
            if sps.count() > 1:
                request.session['selected_sp_title'] = sp.course.long_title(sp.native_language)
            return redirect('main:switchboard')
    else:
        form = SignUpCourseForm (None, course_choices=course_choices)
    context = {'form_title': form_title,
               'course_details': course_details,
               'add_signup_option': True,
               }   
    return render(request, 'main/select_course.html', context)
        

@login_required
def signup_course(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": signup_course started")

    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    # show all available courses. let them select one.

    sp_ids = StudentProgress.objects.filter(user=request.user).values_list('course', flat=True)
    # exclude enrolled courses 
    unsigned_courses = Course.objects.exclude(id__in=sp_ids)
    # exclude courses not offered in their native_languages
    
    course_choices = [] # used by form object
    course_details = [] # used in template
    form_title = "Sign up for a new Course"
    for course in unsigned_courses:
        for nl_id in Student.objects.get(user=request.user).list_native_languages():
            native_language = Language.objects.get(id=nl_id)
            course_title = course.long_title(native_language)
            course_code = "{},{}".format(course.id,nl_id)
            course_health = Course.objects.get_health(course,native_language)
            if (course_health > course.QUALITY['LOW']['value']) or request.user.has_perm('grammar.content_creator'):
                course_choices.append((course_code,course_title))
                course_details.append({
                        'course_code':course_code,
                        'course_title':course_title,
                        'course_health':course_health,
                    })            
    if request.method =='POST':
        form = SignUpCourseForm (request.POST, course_choices=course_choices)
        if form.is_valid():
            [course_id,native_id] = request.POST['selection_code'].split(',')
            course = get_object_or_404(Course, id=course_id)
            native = get_object_or_404(Language, id=native_id) 
            sp = StudentProgress.objects.create(user=request.user, course=course, native_language=native)
            course_name = "{}->{}".format(native.title_in_English(),course.target_language.title_in_English())
            Profile.objects.getval(sp,'course')
            Profile.objects.create_or_update(sp=sp,entry='course',text=request.POST['selection_code'])
            messages.add_message(request, messages.SUCCESS,'You have successfully registered for {}'.format(course_name))
            messages.add_message(request, messages.INFO,'You must log in again to activate the new record')            
            log.info("User " + request.user.username + " registered to course {}".format(course_name))
            for key in list(request.session.keys()):
                if not key.startswith("_"): # don't delete system session keys
                    del request.session[key]
            request.session['sp_pk'] = sp.id # repopulate with new relevant sp
            return redirect('main:switchboard')
    else:
        form = SignUpCourseForm (None, course_choices=course_choices)
    context = {'form_title': form_title,
               'course_details': course_details,
               }     
    return render(request, 'main/select_course.html', context)

@permission_required('main.admin',raise_exception=True)
def integrity_check(request):
    from main.models import Sentence, Comment
    
    log = logging.getLogger(__name__)

    English=Language.objects.get(id=1)

    log.debug('--Checking for comments without language--')
    for comment in Comment.objects.all():
        if comment.language is None:
            log.info("Comment without language - ID:{} Text:{}".format(comment.id,comment.text))
    
    log.debug('--Checking for non-sequential stages for a course --')
    for course in Course.objects.all():
        last_position = -1
        last_stage = -1
        for stage in course.stage_set.all().order_by('position'):
            if stage.position == last_position:
                log.info("Duplicate Stage position for same Course - ID:{} and {} Position:{} Course:{}".format(stage.id,last_stage,stage.position,stage.course))
            last_position = stage.position
            last_stage = stage.id

    from grammar import views as grammar_views 
    grammar_views.integrity_check(request)
    from coach import views as coach_views
    coach_views.integrity_check(request)
    return render(request, 'main/test.html')
    
@permission_required('main.admin',raise_exception=True)
def reset_all(request):
    # delete student progress
    GrammarProgress.objects.all().delete()
    StudentProgress.objects.all().delete()
    
    # reset question scores    
    for q in Question.objects.all():
        q.variance = 250
        q.save() # resets question score to that of grammar
    
    # reset stage and grammar scores
    for s in Stage.objects.all():
        s.position = s.position
        s.save()
        for g in s.grammar_set.all():
            g.score = s.score()
            g.save()

    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + " executed reset_all")    
#    logout(request)
    
#     # Checks initial fixtures
#     try: 
#         gc = GramConcept.objects.get(id=1)
#         gc.title='UNASSIGNED'
#         gc.author=request.user
#         gc.save()
#     except:
#         GramConcept.objects.create(
#             id=1,
#             title='UNASSIGNED',
#             author=request.user
#             )
#     
    return redirect('main:switchboard')

@permission_required('main.admin',raise_exception=True)
def export_turkish_questions(request):    
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": export_turkish_questions started")

    import csv
    file = open('all_turkish_questions.csv','w',encoding='utf-8')
    writer = csv.writer(file,quoting=csv.QUOTE_ALL)
    
    # find every question in DB
    for question in Question.objects.filter(target_language=Language.objects.get(id=2)):
#        row=[question.id,str(question.front).encode('utf-8'),str(question.back).encode('utf-8'),str(question.hint).encode('utf-8')]
        row=[question.id,str(question.front),str(question.back),str(question.hint)]
        writer.writerow(row)
    file.close()
    # To import into Excel, click on Data menu, click on "From Text", import file, newline
        
    return render(request, 'main/test.html')

@permission_required('main.admin',raise_exception=True)
def test(request):
    # will make adjustments to existing DB records to upgrade acc
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ": test view started")

    # create a student record for each user
    for sp in StudentProgress.objects.all():
        if not Student.objects.filter(user=sp.user).exists():
            Student.objects.create(user=sp.user,native_languages=Profile.objects.getval(sp,'native_languages'))

#     sp.mode = 'offer_coaching_tips'
#     sp.phase = 'COACHING'
#     sp.save()     
#     return redirect('main:switchboard')      
    return render(request, 'main/test.html')
        