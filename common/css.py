"""
    CSS: Comma Seperated String library
    Handles translation of strings of comma seperated integers into lists and back
"""

import ast

def literal (text):
    # converts a string representation of a list or element into an actual list or element. 
    # If element(s) are integers, returns them as integers
    #  "['2','3','4']"  => [2, 3, 4]    "['Goat','House']" => ['Goat','House']   "'2'" => 2   "2" => 2   
    try:
        # try to return a list of integers
        return [int(x) for x in ast.literal_eval(text)]
    except:
        try:
            # try to return an integer element
            return int(ast.literal_eval(text))
        except:
            try:
                # try to return a list of strings
                return ast.literal_eval(text)
            except: 
                # just return original string
                return text
    
def to_list (css):
    # converts a css of integers to a list
    return [int(x) for x in css.split(',') if x.strip().isdigit()]

def to_css (list):
    # converts a list to css
    return ",".join(map(str, list))

def add (css, element):
    # add element to css string
    list = to_list(css)
    list.append(element)
    return to_css(list)

def add_unique (css, element):
    # Only add if does not already exist
    dict = {x: 1 for x in to_list(css)}
    dict[element] = 1
    return to_css(dict.keys())

def length (css):
    # returns number of elements
    return len(to_list(css))

def pop (css):
    # returns two items, the first being modified css, the second being the popped value
    first_comma = css.find(',')
    if first_comma == -1:
        new_css = ''
        pop_element = css
    else:
        new_css = css[first_comma+1:]
        pop_element = css[:first_comma]
    return new_css, pop_element
