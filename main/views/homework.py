from .main import mode_completed
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from grammar.views import grammar_progress_table_data
from main.models import Profile
from coach.models import Tip
from django.contrib.auth.decorators import login_required
import logging
from .main import mode_completed, mode_not_completed, get_sp_or_redirect

@login_required
def review_summary(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ":"+ __name__ + " review_summary started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')

    if 'checkin_was_today' not in request.session:
        messages.add_message(request, messages.INFO, 'It is too early for your next check-in, but you can review your progress and homework from here at any time.')

    context = {
            'pronunciation_stage':'Not yet measured',   
            'check_in_possible': (sp.new_check_in_valid())
        }
    
    selected_tip_ids = Profile.objects.getval(sp,'selected_tip_ids')
    if selected_tip_ids:
        context.update({'selected_tips':Tip.objects.filter(id__in=selected_tip_ids)})
    selected_exercise_ids = Profile.objects.getval(sp,'selected_exercise_ids')
    if selected_exercise_ids:
        context.update({'selected_exercises':Tip.objects.filter(id__in=selected_exercise_ids)})        
    context.update(grammar_progress_table_data (request))
    return render(request, 'main/review_summary.html', context)
