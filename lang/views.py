from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
import logging
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages

def splash_view(request):
    request.session.set_test_cookie()
    return render(request, 'splash_page.html')

def no_cookies(request):
    messages.add_message(request, messages.WARNING, '*** In order to use this site you must have cookies enabled in your web browser ***')
    return render(request, 'splash_page.html')
        
@login_required
def logout_view(request):
    logout(request)
    log = logging.getLogger(__name__)
    log.info("Sign-out: " + str(request.user.username))
    return render(request, 'registration/logged_out.html')

def new_login(request):
    log = logging.getLogger(__name__)
    log.info("Sign-in: " + str(request.user.username))
#     request.session['new login']
    return redirect('main:switchboard')
 