from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from main.forms import ContinueForm
from main.views import mode_completed, mode_not_completed, get_sp_or_redirect
from grammar.views import grammar_progress
from grammar.models import FocusGrammarProgress, GrammarProgress
from main.models import Stage, Profile
from django.contrib import messages
import logging
from django.template.response import SimpleTemplateResponse
from common import css

@login_required
def initial_grammar_report(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ":"+ __name__ + " initial_grammar_report started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp = FocusGrammarProgress.objects.get(sp=sp)

    if request.POST: # advancing
        form = ContinueForm(request.POST)
        if form.is_valid():
            form.save(request)
            return mode_completed(request)
    else:
        Profile.objects.create_or_update(sp=sp,entry='initial_grammar_test_completed',value=True)
        messages.add_message(request, messages.SUCCESS, 'Your initial assessment has been completed. You have reached stage ' + fp.stage.title_in(sp.native_language) + '.')
        messages.add_message(request, messages.INFO, 'In future weeks we will review your competency in the following grammars.')
        context = {'continue_button': True,
                   } 
        return grammar_progress(request,context)        

def weekly_grammar_report(request):
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + ":"+ __name__ + " initial_grammar_report started")
    sp,request = get_sp_or_redirect(request)
    if not sp:
        return redirect('main:select_course')
    fp = FocusGrammarProgress.objects.get(sp=sp)

    if request.POST: # advancing
        form = ContinueForm(request.POST)
        if form.is_valid():
            form.save(request)
            return mode_completed(request)
    else:
        context = {'continue_button': True,
                   } 
        return grammar_progress(request,context)        