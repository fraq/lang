from main.models import Comment, Sentence

class TranslatableMixin():
    # we don't define Translatable attributes, they will be taken from elsewhere
    def __str__(self):
        return str(self.title)

    def locate_title_in(self,language):
        if self.title is None:
            return None
        # see if the root is in desired language
        else:
            if self.title.language == language:
                return self.title                
            # otherwise, see if a child is in desired language
            else:
                translated_child=Sentence.objects.filter(parent=self.title, language=language).first()
                if translated_child is not None:
                    return translated_child
                # otherwise return root entry
                else:
                    return self.title

    def title_in(self,languages):
        # languages can be a list of languages in preferred order or a singular element
        try:
            for lang in languages: # try all languages provided in order to search for a suitable option
                title = self.locate_title_in(lang)
                if title is None:
                    result = ''
                else:
                    result = title.text
                    break
        except:
            # languages is not a list, but a singular language
            title = self.locate_title_in(languages)
            if title is None:
                result = ''
            else:
                result = title.text
        return result

    def title_in_English(self):
        from main.models import Language
        return self.title_in(Language.objects.English())
    
    def available_languages(self):
        """Returns all native_languages the title is available in"""
        available = set([self.title.language])
        for lang in Sentence.objects.filter(parent=self.title):
            available.add(lang)        
        return list(available)