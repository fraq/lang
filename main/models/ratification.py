from django.db import models
from django.contrib.auth.models import User
import logging
from tinymce import HTMLField

class Ratification(models.Model):
    """
    Sentences, Questions and other content can undergo an approval process by native speakers. Decisions by native speakers are 
    pointed to by a M2M relationship of the original model (e.g. Sentence model) and the results are stored here.
    
    assement field: 
    * For sentences/questions could be 'A' (approved) or 'R' (rejected).
    * For audio recordings could be 'S5L3' (Sound quality 5/5, Language quality 3/5')
    """
    APPROVE = 'A'
    REJECT = 'R'
    PASS = 'P'
    R_CHOICES = ((APPROVE, 'Approve'), (REJECT, 'Reject'), (PASS, 'Pass'))
    
    assessment = models.CharField(max_length=10)
    alternative = HTMLField(blank=True,help_text='Proposed alternative')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField(auto_now=True)

    def save(self, *args, **kwargs):
        super(Ratification, self).save(*args, **kwargs)
        log = logging.getLogger(__name__)
        log.info("Model Ratification Instance saved:"+str(self))

    def __str__(self):
        return  str([self.id, self.assessment, self.alternative])


    class Meta:
        permissions = (
            ("ratify", "Can ratify content in their native language"),
        )
        